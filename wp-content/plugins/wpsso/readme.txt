=== WPSSO Core | Meta Tags and Schema Structured Data ===
Plugin Name: WPSSO Core
Plugin Slug: wpsso
Text Domain: wpsso
Domain Path: /languages
License: GPLv3
License URI: https://www.gnu.org/licenses/gpl.txt
Assets URI: https://surniaulula.github.io/wpsso/assets/
Tags: woocommerce, structured data, seo, schema, open graph, facebook, linkedin, twitter, google, pinterest, social sharing, meta tags
Contributors: jsmoriss
Requires PHP: 7.0
Requires At Least: 4.5
Tested Up To: 5.7.2
WC Tested Up To: 5.4.1
Stable Tag: 8.33.0

Rank higher and improve click-through-rates by presenting your content at its best on social sites and in search results.

== Description ==

<h3 class="top">THE MOST ADVANCED STRUCTURED DATA PLUGIN FOR WORDPRESS</h3>

<p><img class="readme-icon" src="https://surniaulula.github.io/wpsso/assets/icon-256x256.png"><strong>WPSSO helps you rank higher and improves click-through-rates by presenting your content at its best on social sites and in search results - no matter how URLs are shared, re-shared, messaged, posted, embedded, or crawled.</strong></p>

<p><strong>WPSSO provides meta tags and structured data markup for:</strong></p>

<ul>
	<li>Facebook / Open Graph</li>
	<li>Google Knowledge Graph</li>
	<li>Google Rich Results (aka Rich Snippets)</li>
	<li>LinkedIn / oEmbed Data</li>
	<li>Mobile Web Browsers</li>
	<li>Pinterest Rich Pins</li>
	<li>Twitter Cards</li>
	<li>Schema.org Markup (for 500+ Schema Types)</li>
	<li>WhatsApp and Messaging Apps</li>
	<li>WordPress REST API and More!</li>
</ul>

<p><strong>WPSSO reads existing content, plugin, and service API data:</strong></p>

There's no need to manually re-enter descriptions, titles, product information, or re-select images and videos! WPSSO can read your existing WordPress content, plugin data, and fetch data from remote service APIs (Bitly, Facebook, Shopper Approved, Stamped.io, Vimeo, Wistia, YouTube, and many more).

WPSSO can be your only social and search optimization plugin, or combined to improve the structured data of another SEO plugin (like All in One SEO Pack, Jetpack SEO Tools, Rank Math SEO, SEO Ultimate, SEOPress, The SEO Framework, WP Meta SEO, Yoast SEO, and more).

<p><strong>WPSSO is FAST, reliable, and coded for performance:</strong></p>

WPSSO is coded for SPEED and QUALITY ASSURANCE using advanced WordPress caching techniques, media optimization (ie. image and video SEO, 1:1, 4:3, and 16:9 images for Google, etc.), template validations, discreet contextual notices, and much more.

<h3>Users Love the WPSSO Core Plugin</h3>

&#x2605;&#x2605;&#x2605;&#x2605;&#x2605; - "Unlike competitors, you can literally customize just about every aspect of SEO and Social SEO if you desire to. &#91;...&#93; This plugin has the most complete JSON-LD markup out of any of them, so you won’t have any errors and warnings in search console for WordPress or WooCommerce sites. You can go crazy customizing everything, or you can just set and forget. There aren’t many plugins that allow the best of both worlds." - [kw11](https://wordpress.org/support/topic/most-responsive-developer-ive-ever-seen/)

&#x2605;&#x2605;&#x2605;&#x2605;&#x2605; - "This plugin makes getting sites structured data ready extremely easy, and it works flawlessly without any issues. It shows messages on the top bar every step of the way to alert you of any issues until everything is set up correctly. It made all my ecommerce products pass Google's validation tests. Great work." - [marguy1](https://wordpress.org/support/topic/excellent-plugin-6825/)

&#x2605;&#x2605;&#x2605;&#x2605;&#x2605; - "This plugin has been a lifesaver in terms of making our images and links and descriptions look beautiful everywhere we post them! It’s so easy to use and the preview functions are incredibly helpful." - [lotusblooms](https://wordpress.org/support/topic/love-this-plugin-789/)

&#x2605;&#x2605;&#x2605;&#x2605;&#x2605; - "What a fantastic plugin. If you want to fix all the errors in search console for structured data, this is the plugin to use. Love it." - [goviral](https://wordpress.org/support/topic/makes-schema-so-easy/)

&#x2605;&#x2605;&#x2605;&#x2605;&#x2605; - "This plugin saves me so much time, and it has really lifted my SERP rankings. Most of my keywords I now rank 1-3 position. I also noticed after about a week that my impressions have gone up at least 75%. I upgraded to the pro version which gave me even more options." - [playnstocks](https://wordpress.org/support/topic/excellent-plugin-and-support-200/)

<h3>WPSSO Core Plugin Features</h3>

Provides meta tag and Schema markup for WordPress posts / pages, plugin and theme custom post types, taxonomies, search results, shop pages, and much more.

Enhances oEmbed markup for LinkedIn, and provides additional article and product meta tags for Slack.

Offers optimized image sizes for social sites and search engines:

* Open Graph (Facebook and oEmbed)
* Pinterest Pin It
* Schema 1:1 (Google Rich Results and SEO)
* Schema 4:3 (Google Rich Results and SEO)
* Schema 16:9 (Google Rich Results and SEO)
* Schema Thumbnail Image
* Twitter Summary Card
* Twitter Large Image Summary Card
* **[Premium]** Twitter Player Card

Built-in compatibility with AMP plugins:

* [AMP](https://wordpress.org/plugins/amp/)
* [AMP for WP](https://wordpress.org/plugins/accelerated-mobile-pages/)

Built-in compatibility with caching and optimization plugins:

* Autoptimize
* Cache Enabler
* Comet Cache
* Hummingbird Cache
* LiteSpeed Cache
* Pagely Cache
* Perfect Images + Retina
* SiteGround Cache
* W3 Total Cache (aka W3TC)
* WP Engine Cache
* WP Fastest Cache
* WP Rocket Cache
* WP Super Cache

Built-in compatibility with popular SEO plugins:

* All in One SEO Pack
* Jetpack SEO
* Rank Math SEO
* SEO Ultimate
* SEOPress
* Slim SEO
* Squirrly SEO
* The SEO Framework
* WP Meta SEO
* Yoast SEO
* Yoast WooCommerce SEO

Built-in compatibility for advanced WordPress configurations:

* WordPress MU Domain Mapping
* Network / Multisite Installations

Includes advanced QUALITY ASSURANCE features and options:

* Checks and warns of any missing PHP modules.
* Checks third-party plugin settings for possible conflicts.
* Checks for minimum / maximum image dimensions and aspect ratios.
* Shows notices for missing and required images.
* Validates theme header templates for correct HTML markup.
* Verifies webpage HTML for duplicate meta tags.

The WPSSO Core Standard plugin is designed to satisfy the requirements of most standard WordPress sites. If your site requires additional third-party plugin and service API integration, like WooCommerce shops, embedded video support, or advanced customization features, then you may want to get the [WPSSO Core Premium plugin](https://wpsso.com/extend/plugins/wpsso/) for those additional features.

**[Premium]** Detection of embedded videos in content text with API support for Facebook, Slideshare, Vimeo, Wistia, and Youtube videos.

**[Premium]** Support for the Twitter [Player Card](https://dev.twitter.com/cards/types/player) for embedded videos.

**[Premium]** Optional upscaling of small images to satisfy minimum image size requirements for social sharing and Schema markup.

**[Premium]** URL shortening with Bitly, DLMY.App, Google, Ow.ly, TinyURL, or YOURLS.

**[Premium]** Customize the default Open Graph and Schema document type for posts, pages, custom post types, taxonomy terms (categories, tags, etc.), and user profile pages.

**[Premium]** Customize the post and taxonomy types included in the WordPress sitemap XML.

**[Premium]** Complete Schema JSON-LD markup for WooCommerce products ([WPSSO JSON add-on required](https://wordpress.org/plugins/wpsso-schema-json-ld/)).

**[Premium]** Integrates with other plugins and service APIs for additional image, video, e-Commerce product details, SEO settings, etc. The following modules are included with the Premium version and automatically loaded if/when the supported plugins and/or services are required.

* **Reads data from third-party plugins:** 

	* All in One SEO Pack
	* bbPress
	* BuddyPress
	* Co-Authors Plus
	* Easy Digital Downloads
	* Gravity Forms + GravityView
	* NextCellent Gallery - NextGEN Legacy
	* NextGEN Gallery
	* Perfect WooCommerce Brands
	* Polylang
	* Product GTIN (EAN, UPC, ISBN) for WooCommerce
	* Rate my Post
	* SEOPress
	* Simple Job Board
	* The Events Calendar
	* The SEO Framework
	* WooCommerce
	* WooCommerce Brands
	* WooCommerce Currency Switcher
	* WooCommerce UPC, EAN, and ISBN
	* WooCommerce Show Single Variations
	* WP Job Manager
	* WP Meta SEO
	* WP-PostRatings
	* WP Product Review
	* WP Recipe Maker
	* WPML
	* YITH WooCommerce Brands Add-on
	* Yoast SEO
	* Yotpo Social Reviews for WooCommerce

* **Reads data from remote service APIs:**

	* Bitly
	* DLMY.App
	* Facebook Embedded Videos
	* Gravatar (Author Image)
	* Ow.ly
	* Shopper Approved (Ratings and Reviews)
	* Slideshare Presentations
	* Soundcloud Tracks (for the Twitter Player Card)
	* Stamped.io (Ratings and Reviews)
	* TinyURL
	* Vimeo Videos
	* Wistia Videos
	* WordPress Video Shortcode (and Self-Hosted Videos)
	* Your Own URL Shortener (YOURLS)
	* YouTube Videos and Playlists

<h3>Free Complementary Add-ons</h3>

<p><strong>Do you need even more advanced or unique features?</strong></p>

Activate any of the free complementary add-on(s) you require:

* [WPSSO FAQ Manager](https://wordpress.org/plugins/wpsso-faq/) to manage FAQ categories with Question and Answer pages.
* [WPSSO Inherit Parent Metadata](https://wordpress.org/plugins/wpsso-inherit-parent-meta/) to inherit featured and custom images.
* [WPSSO Mobile App Meta Tags](https://wordpress.org/plugins/wpsso-am/) to manage mobile App information.
* [WPSSO Organization Markup](https://wordpress.org/plugins/wpsso-organization/) to manage multiple Organizations.
* [WPSSO Place and Local SEO Markup](https://wordpress.org/plugins/wpsso-plm/) to manage multiple locations.
* [WPSSO Product Metadata for WooCommerce](https://wordpress.org/plugins/wpsso-wc-metadata/) to add GTIN, GTIN-8, GTIN-12 (UPC), GTIN-13 (EAN), GTIN-14, ISBN, MPN, depth, and volume for WooCommerce products and variations.
* [WPSSO Ratings and Reviews](https://wordpress.org/plugins/wpsso-ratings-and-reviews/) to add ratings in WordPress comments.
* [WPSSO REST API](https://wordpress.org/plugins/wpsso-rest-api/) to add meta tags and Schema markup in REST API queries.
* [WPSSO Ridiculously Responsive Social Sharing Buttons](https://wordpress.org/plugins/wpsso-rrssb/) to add responsive share buttons.
* [WPSSO Schema Breadcrumbs Markup](https://wordpress.org/plugins/wpsso-breadcrumbs/) to add Breadcrumbs markup for Google.
* [WPSSO Schema JSON-LD Markup](https://wordpress.org/plugins/wpsso-schema-json-ld/) to provide Google with Schema markup in its preferred format.
* [WPSSO Shipping Delivery Time for WooCommerce](https://wordpress.org/plugins/wpsso-wc-shipping-delivery-time/) to provide Google with shipping rates and delivery time estimates.
* [WPSSO Strip Schema Microdata](https://wordpress.org/plugins/wpsso-strip-schema-microdata/) to strip incorrect markup from templates.
* [WPSSO Tune WP Image Editors](https://wordpress.org/plugins/wpsso-tune-image-editors/) for better looking WordPress thumbnails.
* [WPSSO User Locale Selector](https://wordpress.org/plugins/wpsso-user-locale/) to switch languages quickly and easily.

== Installation ==

<h3 class="top">Install and Uninstall</h3>

* [Install the WPSSO Core Plugin](https://wpsso.com/docs/plugins/wpsso/installation/install-the-plugin/)
* [Uninstall the WPSSO Core Plugin](https://wpsso.com/docs/plugins/wpsso/installation/uninstall-the-plugin/)

<h3>Plugin Setup</h3>

* [Setup Guide](https://wpsso.com/docs/plugins/wpsso/installation/setup-guide/)
* [Much Better Schema Markup for WooCommerce Products](https://wpsso.com/docs/plugins/wpsso/installation/better-schema-for-woocommerce/)
* [Integration Notes](https://wpsso.com/docs/plugins/wpsso/installation/integration/)
	* [BuddyPress Integration Notes](https://wpsso.com/docs/plugins/wpsso/installation/integration/buddypress-integration/)
	* [WooCommerce Integration Notes](https://wpsso.com/docs/plugins/wpsso/installation/integration/woocommerce-integration/)
* [Troubleshooting Guide](https://wpsso.com/docs/plugins/wpsso/installation/troubleshooting-guide/)
* [Developer Special: Buy one, Get one Free](https://wpsso.com/docs/plugins/wpsso/installation/developer-special-buy-one-get-one-free/)

== Frequently Asked Questions ==

<h3 class="top">Frequently Asked Questions</h3>

* [Does LinkedIn read Facebook / Open Graph meta tags?](https://wpsso.com/docs/plugins/wpsso/faqs/does-linkedin-read-the-open-graph-meta-tags/)
* [How can I fix a ERR_TOO_MANY_REDIRECTS error?](https://wpsso.com/docs/plugins/wpsso/faqs/how-can-i-fix-a-err_too_many_redirects-error/)
* [How can I fix a generic HTTP 500 error?](https://wpsso.com/docs/plugins/wpsso/faqs/how-can-i-fix-a-generic-http-500-error/)
* [How can I fix a PHP fatal "out of memory" error?](https://wpsso.com/docs/plugins/wpsso/faqs/how-can-i-fix-a-php-fatal-out-of-memory-error/)
* [How can I fix an HTTP error when uploading images?](https://wpsso.com/docs/plugins/wpsso/faqs/how-can-i-fix-an-http-error-when-uploading-images/)
* [How can I have smaller dimensions for the default image?](https://wpsso.com/docs/plugins/wpsso/faqs/how-can-i-have-smaller-dimensions-for-the-default-image/)
* [How can I see what the Facebook crawler sees?](https://wpsso.com/docs/plugins/wpsso/faqs/how-can-i-see-what-the-facebook-crawler-sees/)
* [How do I enable WordPress WP_DEBUG?](https://wpsso.com/docs/plugins/wpsso/faqs/how-do-i-enable-wordpress-wp_debug/)
* [How do I fix Google Structured Data &gt; hatom errors?](https://wpsso.com/docs/plugins/wpsso/faqs/how-do-i-fix-google-structured-data-hatom-errors/)
* [How do I remove duplicate meta tags?](https://wpsso.com/docs/plugins/wpsso/faqs/how-do-i-remove-duplicate-meta-tags/)
* [How does WPSSO Core find and select images?](https://wpsso.com/docs/plugins/wpsso/faqs/how-does-wpsso-find-detect-select-images/)
* [How does WPSSO Core find and select videos?](https://wpsso.com/docs/plugins/wpsso/faqs/how-does-wpsso-find-detect-select-videos/)
* [W3C says "there is no attribute 'property'"](https://wpsso.com/docs/plugins/wpsso/faqs/w3c-says-there-is-no-attribute-property/)
* [Why are some HTML elements missing or misaligned?](https://wpsso.com/docs/plugins/wpsso/faqs/why-are-some-html-elements-missing-misaligned-different/)
* [Why does Facebook show the wrong image / text?](https://wpsso.com/docs/plugins/wpsso/faqs/why-does-facebook-show-the-wrong-image-text/)
* [Why does Google Structured Data Test Tool show errors?](https://wpsso.com/docs/plugins/wpsso/faqs/why-does-google-structured-data-testing-tool-show-errors/)
* [Why shouldn't I upload small images to the media library?](https://wpsso.com/docs/plugins/wpsso/faqs/why-shouldnt-i-upload-small-images-to-the-media-library/)

<h3>Notes and Documentation</h3>

* [Developer Resources](https://wpsso.com/docs/plugins/wpsso/notes/developer/)
	* [Constants](https://wpsso.com/docs/plugins/wpsso/notes/developer/constants/)
	* [Filters](https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/)
		* [All Filters](https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/all/)
		* [Filter Examples](https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/examples/)
			* [Define a Custom Post Type (CPT) as a Product](https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/examples/define-a-custom-post-type-cpt-as-a-product/)
			* [Detect YouTube URL Links as Videos](https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/examples/detect-youtube-url-links-as-videos/)
			* [Modify the "article:tag" Keywords / Names](https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/examples/modify-the-articletag-keywords-names/)
			* [Modify the Default Article Section List](https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/examples/modify-the-default-article-sections-list/)
			* [Modify the Sharing URL](https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/examples/modify-the-sharing-url/)
			* [Remove / Fix 'hentry' Errors in your Theme Templates](https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/examples/remove-hentry-from-theme-templates/)
			* [Strip Additional Shortcodes](https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/examples/strip-additional-shortcodes/)
			* [Use the REQUEST_URI for Post URLs](https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/examples/use-the-request_uri-for-post-urls/)
			* [Working with Pre-defined Meta Tags and Custom Post Types](https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/examples/working-with-pre-defined-meta-tags-and-custom-post-types/)
		* [Filters by Category](https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/by-category/)
			* [Head Filters](https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/by-category/head/)
			* [Media Filters](https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/by-category/media/)
			* [Facebook / Open Graph Filters](https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/by-category/open-graph/)
			* [Twitter Card Filters](https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/by-category/twitter-card/)
			* [Webpage Filters](https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/by-category/webpage/)
		* [Other Filters](https://wpsso.com/docs/plugins/wpsso/notes/developer/filters/other/)
	* [The $mod Variable](https://wpsso.com/docs/plugins/wpsso/notes/developer/the-mod-variable/)
* [Inline Variables](https://wpsso.com/docs/plugins/wpsso/notes/inline-variables/)
* [Multisite / Network Support](https://wpsso.com/docs/plugins/wpsso/notes/multisite-network-support/)

== Screenshots ==

01. The essential settings page offers a quick and easy setup.
02. You can easily customize the details of articles, events, e-Commerce products, recipes, reviews, and much more.
03. The preview tab allows you to quickly preview an example social share.

== Changelog ==

<h3 class="top">Release Schedule</h3>

<p>New versions of the plugin are released approximately every week (more or less). New features are added, tested, and released incrementally, instead of grouping them together in a major version release. When minor bugs fixes and/or code improvements are applied, new versions are also released. This release schedule keeps the code stable and reliable, at the cost of more frequent updates.</p>

<p>See <a href="https://en.wikipedia.org/wiki/Release_early,_release_often">release early, release often (RERO) software development philosophy</a> on Wikipedia for more information on the benefits of smaller / more frequent releases.</p>

<h3>Version Numbering</h3>

Version components: `{major}.{minor}.{bugfix}[-{stage}.{level}]`

* {major} = Major structural code changes / re-writes or incompatible API changes.
* {minor} = New functionality was added or improved in a backwards-compatible manner.
* {bugfix} = Backwards-compatible bug fixes or small improvements.
* {stage}.{level} = Pre-production release: dev &lt; a (alpha) &lt; b (beta) &lt; rc (release candidate).

<h3>Standard Version Repositories</h3>

* [GitHub](https://surniaulula.github.io/wpsso/)
* [WordPress.org](https://plugins.trac.wordpress.org/browser/wpsso/)

<h3>Development Updates for Premium Users</h3>

<p>Development, alpha, beta, and release candidate updates are available for Premium users.</p>

<p>Under the SSO &gt; Update Manager settings page, select the "Development and Up" version filter for WPSSO Core and all its extensions (to satisfy any version dependencies). Save the plugin settings, and click the "Check for Plugin Updates" button to fetch the latest / current WPSSO version information. When new Development versions are available, they will automatically appear under your WordPress Dashboard &gt; Updates page. You can always re-select the "Stable / Production" version filter at any time to re-install the last stable / production version of a plugin.</p>

<h3>Changelog / Release Notes</h3>

**Version 8.34.0-b.1 (2021/06/29)**

* **New Features**
	* None.
* **Improvements**
	* Head markup cache is now disabled if a caching plugin or service is detected.
* **Bugfixes**
	* None.
* **Developer Notes**
	* None.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v4.5.

**Version 8.33.0 (2021/06/27)**

* **New Features**
	* Added a new module for Stamped.io ratings and reviews in the WPSSO Core Premium plugin.
* **Improvements**
	* Added new options under the SSO &gt; Advanced Settings &gt; Service APIs &gt; Ratings and Reviews tab:
		* Ratings and Reviews Service
		* Stamped.io Store Hash
		* Stamped.io API Key Public
	* Renamed the "Video API Info Cache Expiry" option to "API Response Cache Expiry".
* **Bugfixes**
	* None.
* **Developer Notes**
	* Added supported caching plugins to the array returned by `WpssoCheck->get_avail()`.
	* Renamed the 'wpsso_cache_expire_video_info' filter to 'wpsso_cache_expire_api_response'.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v4.5.

**Version 8.32.0 (2021/06/23)**

* **New Features**
	* Added a new SSO &gt; Advanced Settings &gt; Caching &gt; Cache Attachment Markup option.
* **Improvements**
	* Removed the "Clear All Caches on Activate" option (now always enabled).
	* Removed the "Clear All Caches on Deactivate" option (now always disabled for WPSSO Core and always enabled for add-ons).
* **Bugfixes**
	* Fix to remove the Document SSO metabox from the WooCommerce orders and coupons editing pages.
* **Developer Notes**
	* Added new constants:
		* `WPSSO_CACHE_REFRESH_MAX_TIME`
		* `WPSSO_CACHE_REFRESH_SLEEP_TIME`
	* Added `_deprecated_function()` calls to all deprecated functions and methods.
	* Refactored the `WpssoUtil->get_cache_exp_secs()` method.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v4.5.

**Version 8.31.0 (2021/06/18)**

* **New Features**
	* Added a new SSO &gt; Advanced Settings &gt; Caching &gt; Cache Date Archive Markup option.
* **Improvements**
	* Added a snackbar reminder in the block editor in case there are any important error messages under the SSO notification icon.
* **Bugfixes**
	* None.
* **Developer Notes**
	* None.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v4.5.

**Version 8.30.2 (2021/06/16)**

* **New Features**
	* None.
* **Improvements**
	* Moved the SSO &gt; Features metaboxes to the SSO &gt; Dashboard page.
	* Added an error notice message in case the `is_admin_bar_showing()` function returns false.
* **Bugfixes**
	* Fixed a non-working and duplicated copy-to-clipboard icon issue.
* **Developer Notes**
	* None.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v4.5.

**Version 8.30.1 (2021/06/12)**

* **New Features**
	* None.
* **Improvements**
	* None.
* **Bugfixes**
	* Fixed the missing text length message for textarea fields in the Document SSO metabox.
* **Developer Notes**
	* None.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v4.5.

**Version 8.30.0 (2021/06/08)**

* **New Features**
	* None.
* **Improvements**
	* Added a thumbnail to all Image ID options.
	* Updated the YouTube video API URL query arguments (Premium version).
* **Bugfixes**
	* Fixed an HTTP 404 error for YouTube video API calls (Premium version).
* **Developer Notes**
	* Added new `SucomUtil::sanitize_css_class()` and `SucomUtil::sanitize_css_id()` methods.
	* Renamed all '\*_img_id_pre' option keys to '\*_img_id_lib'.
	* Refactored the `WpssoMedia::get_default_images()` method.
	* Refactored almost all methods in the SucomForm class.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v4.5.

**Version 8.29.0 (2021/05/30)**

Please note that the WP Ultimate Recipe plugin is deprecated and support for this plugin has been removed from the Premium version.

* **New Features**
	* None.
* **Improvements**
	* Removed the deprecated WP Ultimate Recipe integration module (Premium version).
	* Updated the WP Recipe Maker integration module to include instruction sections and images (Premium version).
* **Bugfixes**
	* None.
* **Developer Notes**
	* None.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v4.5.

**Version 8.28.2 (2021/05/15)**

* **New Features**
	* None.
* **Improvements**
	* Added cache refresh when a WooCommerce product changes on the front-end - for example, from in stock to out of stock (Premium version).
* **Bugfixes**
	* None.
* **Developer Notes**
	* Added a hook for the 'woocommerce_after_product_object_save' action in the WooCommerce integration module (Premium version).
* **Requires At Least**
	* PHP v7.0.
	* WordPress v4.5.

**Version 8.28.1 (2021/05/05)**

* **New Features**
	* None.
* **Improvements**
	* Updated the SSO &gt; Advanced &gt; Plugin Settings &gt; Image Sizes tab information text.
	* Moved Priority Media default image selection to Premium version.
* **Bugfixes**
	* Fixed the missing jQuery sucomTextLen() function calls to display text limits after saving/updating in the block editor.
* **Developer Notes**
	* Removed the WpssoEdit->filter_metabox_sso_media_rows() method.
	* Renamed Schema image option keys ratio delimiter from '_' to 'x'.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v4.5.

**Version 8.28.0 (2021/04/30)**

* **New Features**
	* Added a new SSO &gt; Advanced Settings &gt; WordPress Sitemaps metabox with options to customize the post and taxonomy types included in the WordPress sitemap XML.
* **Improvements**
	* Updated the metabox tabs jQuery to scroll the tabbed container into view if/when necessary.
* **Bugfixes**
	* None.
* **Developer Notes**
	* None.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v4.5.

**Version 8.27.0 (2021/04/24)**

* **New Features**
	* Removed support for the rtMedia plugin (Premium version).
* **Improvements**
	* Fine-tuned the CSS for settings and Document SSO metabox tabs.
	* Renamed the "SSO" menu item to "SSO (Social and Search Optimization)".
* **Bugfixes**
	* Fixed merging of new plugin / add-on options keys during update.
* **Developer Notes**
	* Refactored the Yoast SEO integration module (Premium version).
	* Added a new 'wpsso_wpseo_replace_vars' filter for the Yoast SEO integration modules.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v4.5.

**Version 8.26.3 (2021/04/17)**

* **New Features**
	* None.
* **Improvements**
	* Included support for custom post types that do not appear in the WordPress menu.
	* Allowed 'expired' posts to have an 'article:published_time' meta tag (expired posts are assumed to have been previously published).
* **Bugfixes**
	* None.
* **Developer Notes**
	* Added debug messages to the WpssoPost, WpssoTerm, and WpssoUser `add_meta_boxes()` method.
	* Added a new `SucomUtilWP::get_available_languages()` method.
	* Added a new `SucomForm->get_checklist_post_tax_user()` method.
	* Refactored the `SucomForm->get_checklist_post_types()` method.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v4.5.

**Version 8.26.2 (2021/04/13)**

* **New Features**
	* None.
* **Improvements**
	* Added a new SSO &gt; Tools and Actions &gt; Clear Failed URL Connections button.
* **Bugfixes**
	* Fixed the default size values included in help text for the Pinterest Pin It image size option.
* **Developer Notes**
	* Refactored the `WpssoUtilCache->clear_cache_files()` method.
	* Added a new `WpssoUtilCache->count_cache_files()` method.
	* Added a new `WpssoUtilCache->get_cache_files()` method.
	* Added a new `WpssoUtilCache->clear_ignored_urls()` method.
	* Added a new `WpssoUtilCache->count_ignored_urls()` method.
	* Added a new `SucomCache->clear_ignored_urls()` method.
	* Added a new `SucomCache->count_ignored_urls()` method.
	* Added a new `SucomCache->get_ignored_urls()` method.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v4.5.

**Version 8.26.1 (2021/04/02)**

* **New Features**
	* None.
* **Improvements**
	* Moved the SSO &gt; Image Sizes settings to the SSO &gt; Advanced Settings &gt; Image Sizes tab.
	* Moved the SSO &gt; Document Types settings to the SSO &gt; Advanced Settings &gt; Document Types metabox.
	* Removed the Add Pinterest "nopin" to Images option from the SSO &gt; Essential Settings page (available in General Settings).
	* Removed the Add Hidden Image for Pinterest option from the SSO &gt; Essential Settings page (available in General Settings).
* **Bugfixes**
	* Fixed an incorrect prefix for site verification meta tag names.
* **Developer Notes**
	* Added a new 'wpsso_og_add_wc_mt_rating' filter.
	* Added a new 'wpsso_og_add_wc_mt_reviews' filter.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v4.5.

**Version 8.25.2 (2021/03/30)**

* **New Features**
	* None.
* **Improvements**
	* Added a 'wpsso_add_schema_head_attributes' filter check (true by default) before validating theme header templates.
* **Bugfixes**
	* Fixed plugin option pattern matching for taxonomy names ending with '_time'.
* **Developer Notes**
	* Added a PHP class object check before getting the post content reading time.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v4.5.

**Version 8.25.1 (2021/03/19)**

* **New Features**
	* None.
* **Improvements**
	* Added an `is_embed()` check for the robots 'noindex' default.
* **Bugfixes**
	* None.
* **Developer Notes**
	* None.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v4.5.

**Version 8.25.0 (2021/03/11)**

* **New Features**
	* None.
* **Improvements**
	* Added a new option in the Document SSO metabox for Open Graph articles:
		* Est. Reading Time
	* Added a new option under the SSO &gt; Advanced Settings &gt; Plugin Admin tab:
		* Use Local Plugin Translations
	* Added new options under the SSO &gt; General Settings &gt; Social and Search Sites &gt; Other Sites tab:
		* Ahrefs Website Verification ID
		* Baidu Website Verification ID
		* Bing Website Verification ID
		* Yandex Website Verification ID
	* Moved customization of image sizes to the Premium version. 
* **Bugfixes**
	* Fixed plugin conflict detection with All In One SEO v4.0.
* **Developer Notes**
	* None.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v4.5.

**Version 8.24.0 (2021/03/04)**

* **New Features**
	* Added theme templates for iframe embed content with additional support for custom image URLs.
* **Improvements**
	* None.
* **Bugfixes**
	* None.
* **Developer Notes**
	* Added a new lib/theme-compat/embed.php template.
	* Added a new lib/theme-compat/embed-content.php template.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v4.5.

**Version 8.23.0 (2021/02/25)**

* **New Features**
	* None.
* **Improvements**
	* Added business page URL options in the SSO &gt; Social Pages settings page:
		* Medium Business Page URL
		* TikTok Business Page URL
	* Added custom contact options in the SSO &gt; Advanced settings page (Premium version):
		* Medium URL
		* TikTok URL
	* Updated the banners and icons of WPSSO Core and its add-ons.
* **Bugfixes**
	* None.
* **Developer Notes**
	* None.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v4.5.

**Version 8.22.0 (2021/02/09)**

* **New Features**
	* None.
* **Improvements**
	* None.
* **Bugfixes**
	* None.
* **Developer Notes**
	* Updated product offer methods in `WpssoSchema` for WPSSO JSON v4.14.0:
		* Added a new `add_offers_data()` method.
		* Renamed the `add_aggregate_offer_data()` method to `add_offers_aggregate_data()`.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v4.5.

**Version 8.21.0 (2021/01/30)**

* **New Features**
	* None.
* **Improvements**
	* None.
* **Bugfixes**
	* None.
* **Developer Notes**
	* Added new functions to retrieve Facebook / Open Graph image URLs:
		* `wpsso_get_mod_og_image_url()`
		* `wpsso_get_post_og_image_url()`
		* `wpsso_get_term_og_image_url()`
		* `wpsso_get_user_og_image_url()`
	* Added a new `SucomUtil::get_first_og_image_url()` method.
	* Refactored error handling and error messages in URL shortening classes (Premium version).
	* Refactored the `WpssoErrorException` class and added a new `SucomErrorException` class.
	* Refactored the `WpssoCache->add_ignored_url()` method to report cURL and SSL error messages.
	* Removed the 'wpsso_version_updates' action.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v4.5.

**Version 8.20.0 (2021/01/21)**

* **New Features**
	* None.
* **Improvements**
	* Added support for images in WooCommerce product reviews (Premium version).
* **Bugfixes**
	* Fixed jQuery "document ready" event incompatibility with the block editor.
* **Developer Notes**
	* Added a new `WpssoComment` class in lib/comment.php.
	* Added support for 'is_comment' in `WpssoUtil->get_type_url()`.
	* Added support for 'reviews-images' comment metadata in `WpssoWpMeta->get_mt_comment_review()`.
	* Renamed `WpssoPost->get_og_type_reviews()` to `WpssoPost->get_mt_reviews()`.
	* Moved `WpssoPost->get_og_comment_review()` to `WpssoWpMeta->get_mt_comment_review()`.
	* Changed `jQuery( document ).on( 'ready' )` event handlers to `jQuery()` for jQuery v3.0 compatibility.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v4.5.

**Version 8.19.4 (2021/01/17)**

* **New Features**
	* None.
* **Improvements**
	* None.
* **Bugfixes**
	* Fixed relocation of toolbar notices in WooCommerce 4.9.0 product pages.
* **Developer Notes**
	* None.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v4.5.

**Version 8.19.3 (2021/01/16)**

* **New Features**
	* None.
* **Improvements**
	* None.
* **Bugfixes**
	* None.
* **Developer Notes**
	* Updated the jQuery event to update toolbar notices from `on( 'ready' )` to `on( 'load' )`.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v4.5.

**Version 8.19.2 (2021/01/11)**

* **New Features**
	* None.
* **Improvements**
	* None.
* **Bugfixes**
	* None.
* **Developer Notes**
	* Updated `jQuery( document ).ready()` calls to `jQuery( document ).on( 'ready' )`.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v4.5.

**Version 8.19.1 (2021/01/07)**

* **New Features**
	* None.
* **Improvements**
	* Sorted transient labels in the SSO &gt; Dashboard &gt; Cache Status metabox.
* **Bugfixes**
	* Fix to allow the new "Disable Cache for Debugging" option to be changed in the Standard version.
* **Developer Notes**
	* None.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v4.5.

**Version 8.19.0 (2020/12/29)**

* **New Features**
	* None.
* **Improvements**
	* Added a new "Disable Cache for Debugging" option in the SSO &gt; Advanced Settings page.
* **Bugfixes**
	* Fixed a missing 'article:modified_time' meta tag value.
* **Developer Notes**
	* Added a `Wpsso->get_const_status_bool()` method.
	* Added a `Wpsso->get_const_status_transl()` method.
	* Added a `WpssoUtil->rename_options_by_ext()` method.
	* Added a `WpssoUtil->rename_options_keys()` method.
	* Added a `WpssoUtilCache->expire_zero_filters()` method.
	* Added a `WpssoWpMeta->is_upgrade_options_required()` method.
	* Added support for a new WPSSO_CACHE_DISABLE constant.
	* Refactored the `WpssoWpMeta->upgrade_options()` method.
	* Removed the `WpssoUtil->disable_cache_filters()` method.
	* Removed the `WpssoUtil->rename_opts_by_ext()` method.
	* Removed the `SucomUtil::rename_keys()` method.
	* Renamed the WPSSO_CACHEDIR constant to WPSSO_CACHE_DIR.
	* Renamed the WPSSO_CACHEURL constant to WPSSO_CACHE_URL.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v4.5.

**Version 8.18.0 (2020/12/17)**

* **New Features**
	* None.
* **Improvements**
	* Updated product information text in the Document SSO metabox for WooCommerce (Premium version).
* **Bugfixes**
	* None.
* **Developer Notes**
	* Added a minimum version check for WooCommerce detection in the `WpssoCheck->get_avail()` method.
	* Added `$single = false` argument to the `WpssoSchema->get_json_data()` method.
	* Added `$output = 'objects'` argument to the `WpssoPost->get_primary_terms()` method.
	* Added a new `WpssoPost->get_default_term_id()` method.
	* Refactored the `WpssoPost->get_primary_term_id()` method.
	* Refactored the `WpssoPost->get_primary_terms()` method.
	* Renamed the `SucomUtil::get_site_url()` method to `SucomUtil::get_home_url()`.
	* Removed the `SucomUtil::get_query_salt()` method.
	* Removed deprecated methods from 2019.
* **Requires At Least**
	* PHP v7.0.
	* WordPress v4.5.

**Version 8.17.1 (2020/12/13)**

* **New Features**
	* None.
* **Improvements**
	* None.
* **Bugfixes**
	* Fixed an incorrect 'wpsso_post_url' argument count in the NextGEN Gallery integration module (Premium version).
* **Developer Notes**
	* None.
* **Requires At Least**
	* PHP v5.6.
	* WordPress v4.5.

**Version 8.17.0 (2020/12/11)**

* **New Features**
	* None.
* **Improvements**
	* None.
* **Bugfixes**
	* Fixed canonical and sharing URL pagination.
	* Fixed Schema SearchResultsPage markup to use the search query.
* **Developer Notes**
	* Added new `$mod` array elements:
		* 'query_vars'
		* 'is_404'
		* 'is_search'
		* 'is_archive'
		* 'is_date'
		* 'is_year'
		* 'is_month'
		* 'is_day'
	* Added a new `SucomUtilWP::doing_ajax()` method.
	* Added a new `SucomUtilWP::doing_cron()` method.
	* Added a new `SucomUtilWP::doing_autosave()` method.
	* Refactored the WpssoUtil->get_page_url() method.
	* Refactored the WpssoUtil->get_url_paged() method.
	* Refactored the WpssoUtil->get_request_url() method.
	* Refactored and moved `WpssoUtil->get_page_posts_mods()` to `WpssoPage->get_posts_mods()`.
	* Removed the 'wpsso_server_request_url_cache_disabled' filter.
	* Removed the `$add_page` argument from the following filters:
		* 'wpsso_post_url'
		* 'wpsso_home_url'
		* 'wpsso_term_url'
		* 'wpsso_user_url'
		* 'wpsso_search_url'
		* 'wpsso_archive_page_url'
	* Removed the `$posts_per_page` and `$paged` arguments from the following methods:
		* `WpssoPost->get_posts_ids()`
		* `WpssoTerm->get_posts_ids()`
		* `WpssoUser->get_posts_ids()`
	* Removed the `$posts_per_page` argument from the following methods:
		* `WpssoSchema->add_itemlist_data()`
		* `WpssoSchema->add_posts_data()`
	* Removed the `WpssoSchema->add_page_links()` method.
	* Removed the `$wpsso_paged` global variable.
	* Renamed the WPSSO_SCHEMA_REVIEWS_PER_PAGE_MAX constant to WPSSO_SCHEMA_REVIEWS_MAX.
	* Updated the 'pre_update_option_active_plugins' filter hook fallback to sorting like WordPress (instead of leaving the order as-is).
* **Requires At Least**
	* PHP v5.6.
	* WordPress v4.5.

**Version 8.16.0 (2020/12/08)**

* **New Features**
	* None.
* **Improvements**
	* Renamed the "Product Category" option to "Product Type" in the Document SSO metabox.
	* Added a new "Primary Category" option in the Document SSO metabox.
	* Added support for the Yoast SEO primary category value (Premium version).
	* Added support for the SEO Framework primary category value (Premium version).
	* Added support for the SEOPress primary category value (Premium version).
	* Added a new integration module for Rank Math SEO to provide a primary category value (Premium version).
* **Bugfixes**
	* Fixed a duplicate `meta name="author"` meta tag from SEOPress.
* **Developer Notes**
	* Refactored the `WpssoPost->get_primary_term_id()` method.
	* Added a new `WpssoPost->get_primary_terms()` method.
	* Added a new `wpsso_get_post_primary_category()` function.
	* Added a new 'wpsso_primary_tax_slug' filter.
	* Added a new 'wpsso_primary_term_id' filter.
	* Added a new 'wpsso_primary_terms' filter.
* **Requires At Least**
	* PHP v5.6.
	* WordPress v4.5.

**Version 8.15.0 (2020/12/05)**

* **New Features**
	* None.
* **Improvements**
	* Added a new SSO &gt; Advanced Settings &gt; Services API metabox.
* **Bugfixes**
	* None.
* **Developer Notes**
	* Added a new `WpssoPost->get_primary_term_id()` method.
* **Requires At Least**
	* PHP v5.6.
	* WordPress v4.4.

**Version 8.14.1 (2020/11/30)**

* **New Features**
	* None.
* **Improvements**
	* Improved suggested add-on notifications for the WooCommerce plugin.
* **Bugfixes**
	* Fixed an "undeclared static property: SucomUtilRobots::$directives" error.
* **Developer Notes**
	* None.
* **Requires At Least**
	* PHP v5.6.
	* WordPress v4.4.

**Version 8.14.0 (2020/11/26)**

* **New Features**
	* Added WP Sitemaps filters to add article modified dates, and exclude posts / pages, terms, and users with 'noindex' checked under the Document SSO &gt; Robots Meta tab.
* **Improvements**
	* Added a possible 'disabled' status light (Yellow) in the SSO &gt; Features Status page.
	* Added new features in the SSO &gt; Features Status &gt; Standard Features metabox:
		* Link Relation URL Tags
		* oEmbed Response Enhancements
		* Pinterest / SEO Meta Name Tags
		* Post, Term, and User Robots Meta
		* WP Sitemaps Enhancements
* **Bugfixes**
	* None.
* **Developer Notes**
	* Added a new WpssoUtilRobots class.
	* Added a new SucomUtilRobots class (extended by WpssoUtilRobots).
	* Added a new WpssoProUtilCheckImgDims class (Premium version).
	* Added an update option 'active_plugins' filter to sort the WPSSO Core plugin before its add-ons.
	* Added a new `Wpsso->id` variable to replace `Wpsso->lca` (now deprecated).
	* Removed the `WpssoConfig::$cf[ 'lca' ]` array element (deprecated in 2017).
* **Requires At Least**
	* PHP v5.6.
	* WordPress v4.4.

**Version 8.13.0 (2020/11/20)**

This release includes a new "Validators" toolbar menu and several changes to the Advanced Settings page.

* **New Features**
	* Added a "Validators" menu in the admin toolbar.
* **Improvements**
	* Added a "Show Validators Toolbar Menu" option under the new SSO &gt; Advanced Settings &gt; Interface tab.
	* Moved all options from the SSO &gt; Advanced Settings &gt; Table Columns tab to the new Interface tab (removing the Table Columns tab).
	* Moved all options from the SSO &gt; Advanced Settings &gt; Content and Text tab to the Integration tab (removing the Content and Text tab).
	* Moved the "Default Currency" option to the SSO &gt; General Settings &gt; Site Information tab.
	* Moved the "Check for Embedded Media" options to the SSO &gt; Advanced Settings &gt; Service APIs tab.
	* Moved the "Options to Show by Default" options to the new SSO &gt; Advanced Settings &gt; Interface tab.
	* Moved the "Show Document SSO Metabox" option to the new SSO &gt; Advanced Settings &gt; Interface tab.
	* Moved the "Import Yoast SEO Social Meta" and "Show Yoast SEO Import Details" options to the SSO &gt; Advanced Settings &gt; Integration tab.
	* Removed the "Notification System" option.
	* Removed the "&lt;head&gt; Attributes Filter Hook" option.
	* Removed the "&lt;html&gt; Attributes Filter Hook" option.
* **Bugfixes**
	* None.
* **Developer Notes**
	* None.
* **Requires At Least**
	* PHP v5.6.
	* WordPress v4.4.

**Version 8.12.1 (2020/11/13)**

This release adds a new SSO &gt; Features Status page and integration module for SEOPress (Premium version).

* **New Features**
	* None.
* **Improvements**
	* Added a new SSO &gt; Features Status page.
	* Added an integration module for SEOPress (Premium version).
	* Replaced the status light images in the SSO &gt; Features Status page by CSS background colors.
* **Bugfixes**
	* None.
* **Developer Notes**
	* Added a new lib/conflict-seo.php library file.
	* Added a new `WpssoWpMeta::get_mod_meta()` method for SEO integration modules.
	* Re-ordered the default options in lib/config.php.
* **Requires At Least**
	* PHP v5.6.
	* WordPress v4.4.

**Version 8.11.2 (2020/11/07)**

* **New Features**
	* None.
* **Improvements**
	* The "Enforce Image Dimension Checks" option is now disabled by default.
* **Bugfixes**
	* Fixed an undefined index streamingData error in cases where YouTube video details are incomplete or could not be retrieved.
* **Developer Notes**
	* Added new `SucomAddon->get_ext()` and `SucomAddon->get_p_ext()` methods to retrieve those property values.
* **Requires At Least**
	* PHP v5.6.
	* WordPress v4.4.

**Version 8.11.1 (2020/10/31)**

* **New Features**
	* None.
* **Improvements**
	* None.
* **Bugfixes**
	* Fixed incorrect array type casting when retrieving custom event or job options.
* **Developer Notes**
	* None.
* **Requires At Least**
	* PHP v5.6.
	* WordPress v4.4.

**Version 8.11.0 (2020/10/29)**

* **New Features**
	* None.
* **Improvements**
	* Added extra article and product Twitter Card meta tags for Slack.
* **Bugfixes**
	* Fixed formatting of the Schema 'cutoffTime', 'opens', and 'closes' property values.
* **Developer Notes**
	* Refactored the `WpssoTwitter->get_array()` method.
* **Requires At Least**
	* PHP v5.6.
	* WordPress v4.4.

**Version 8.10.0 (2020/10/28)**

* **New Features**
	* None.
* **Improvements**
	* Added 'businessDays' and 'cutoffTime' to Schema ShippingDeliveryTime markup for the [WPSSO Shipping Delivery Time for WooCommerce add-on](https://wordpress.org/plugins/wpsso-wc-shipping-delivery-time/).
* **Bugfixes**
	* None.
* **Developer Notes**
	* Added a new 'wpsso_schema_type_for_post_type_{post_type}' filter.
	* Added a new `WpssoSchemaSingle::get_opening_hours_data()` method.
	* Removed arguments from the `WpssoUtil->add_plugin_image_sizes()` method as they are not required.
	* Updated the `SucomUtil::preg_grep_keys()` method to handle a replacement string or array.
	* Shortened Schema '@id' property values by removing the home URL prefix.
* **Requires At Least**
	* PHP v5.6.
	* WordPress v4.4.

**Version 8.9.0 (2020/10/24)**

* **New Features**
	* None.
* **Improvements**
	* Updated the markdown library used to parse WordPress readme files.
	* Added a check for the post status before retrieving Shopper Approved ratings and reviews (Premium version).
* **Bugfixes**
	* None.
* **Developer Notes**
	* Re-added the `WpssoUtil::save_all_times()` deprecated method for old add-ons.
	* Refactored lib/ext/markdown.php to use lib/ext/markdown/MarkdownExtra.inc.php.
	* Refactored the `SucomForm->get_select_timezone()` method.
	* Added a new `SucomUtilWP::get_timezones()` method.
	* Added a new `SucomUtilWP::get_default_timezone()` method.
* **Requires At Least**
	* PHP v5.6.
	* WordPress v4.4.

**Version 8.8.1 (2020/10/17)**

* **New Features**
	* Added a new WPSSO Shipping Delivery Time for WooCommerce add-on.
* **Improvements**
	* None.
* **Bugfixes**
	* Fixed backwards compatibility with older 'init_objects' and 'init_plugin' action arguments.
* **Developer Notes**
	* Added a new WpssoAddOn class in lib/abstracts/add-on.php.
	* Added a new SucomAddOn class in lib/abstracts/com/add-on.php.
* **Requires At Least**
	* PHP v5.6.
	* WordPress v4.4.

**Version 8.7.1 (2020/10/04)**

* **New Features**
	* None.
* **Improvements**
	* Added a check for WooCommerce shipping requirements (ie. coupon and/or minimum amount) when getting shipping rates (Premium version).
* **Bugfixes**
	* None.
* **Developer Notes**
	* None.
* **Requires At Least**
	* PHP v5.6.
	* WordPress v4.4.

**Version 8.7.0 (2020/10/02)**

**Added support for the new Schema shippingDetails property in Schema Product offers for WooCommerce products. Note that [Google is now showing shipping costs from Schema markup in search results](https://webmasters.googleblog.com/2020/09/new-schemaorg-support-for-retailer.html).**

* **New Features**
	* None.
* **Improvements**
	* Added support for the new Schema shippingDetails property in Schema Product offers for WooCommerce products, including shippingRate values by currency, and shippingDestination with countries, states, and postal code limits (Premium version).
* **Bugfixes**
	* Fixed a bug that prevented abbreviated Schema enumerations from being detected (ie. New and NewCondition as abbreviations for https://schema.org/NewCondition).
* **Developer Notes**
	* Deprecated the `wpsso_is_mobile()` function.
	* Deprecated the `SucomUtil::is_mobile()` method.
	* Deprecated the `SucomUtil::is_desktop()` method.
	* Removed the lib/ext/mobile-detect.php library file.
* **Requires At Least**
	* PHP v5.6.
	* WordPress v4.4.

**Version 8.6.0 (2020/09/25)**

**The minimum WordPress version has been increased from v4.2 to v4.4.**

* **New Features**
	* None.
* **Improvements**
	* Added a new "Webpage Document Title" option under the SSO &gt; Advanced Settings &gt; Content and Text tab.
	* Added WooCommerce product shipping data (zone, class, methods, and locations) for the WPSSO JSON v4.4.0 add-on (Premium version).
* **Bugfixes**
	* Fixed incorrect Schema AggregateRating meta tag markup when the WPSSO JSON add-on is not active.
	* Fixed an "undeclared static property" error in the SSO &gt; Dashboard page.
* **Developer Notes**
	* None.
* **Requires At Least**
	* PHP v5.6.
	* WordPress v4.4.

**Version 8.5.1 (2020/09/18)**

**The SSO &gt; Setup Guide content is now available for translation. If you are fluent in a language other than English, and would like to assist translating the WPSSO Core option labels, help text, notice messages, or Setup Guide, [you can contribute to the translation of WPSSO Core here](https://translate.wordpress.org/projects/wp-plugins/wpsso/).**

* **New Features**
	* None.
* **Improvements**
	* Additional translation string updates for the SSO &gt; Setup Guide.
	* Added an acknowledgement message when the Site Address URL value has changed.
	* Added new Schema Review sub-types:
		* https://schema.org/CriticReview
		* https://schema.org/EmployerReview
		* https://schema.org/MediaReview
		* https://schema.org/Recommendation
		* https://schema.org/UserReview
* **Bugfixes**
	* Fixed All in One SEO Pack plugin detection by using the "AIOSEOP_Core" class name.
	* Fixed Squirrly SEO conflict notification texts and links for the latest version of Squirrly SEO.
* **Developer Notes**
	* Added enqueue recursion to work around the Squirrly SEO `fixEnqueueErrors()` and `clearStyles()` methods that break toolbar notifications.
	* Added a new WpssoAdminFilters class in lib/admin-filters.php.
	* Added a new WPSSO_ADMIN_SCRIPTS_PRIORITY constant.
	* Added a new WPSSO_BLOCK_ASSETS_PRIORITY constant.
	* Renamed the WPSSO_SEO_SEED_FILTERS_PRIORITY constant to WPSSO_SEO_SEED_PRIORITY.
* **Requires At Least**
	* PHP v5.6.
	* WordPress v4.2.

**Version 8.5.0 (2020/09/15)**

**The SSO &gt; Setup Guide content is now available for translation and the complete French translation of the Setup Guide will be available shortly. If you are fluent in a language other than English, and would like to assist translating the WPSSO Core option labels, help text, notice messages, or Setup Guide, [you can contribute to the translation of WPSSO Core here](https://translate.wordpress.org/projects/wp-plugins/wpsso/).**

* **New Features**
	* None.
* **Improvements**
	* Added translation support for the SSO &gt; Setup Guide page.
	* Moved the SSO &gt; Advanced Settings &gt; Document Types metabox to a new SSO &gt; Document Types settings page.
* **Bugfixes**
	* Fixed an incorrect "The value of option must be numeric" error message for 'robots_max_snippet' and 'robots_max_video_preview' when saving posts.
* **Developer Notes**
	* Moved extracted translation strings from lib/gettext-*.php files to a new gettext/ folder.
	* Added a new gettext/gettext-html-setup.php file to translate the Setup Guide.
* **Requires At Least**
	* PHP v5.6.
	* WordPress v4.2.

**Version 8.4.1 (2020/09/11)**

**This release adds a new "Robots Meta" tab in the Document SSO metabox with options for Google Search and other search engine robots.**

**This release also fixes an important bug, introduced in v8.3.0, that prevented getting archive post objects when not in an archive page.**

* **New Features**
	* Added a new "Robots Meta" tab in the Document SSO metabox with options for Google Search:
		* No Archive
		* No Follow
		* No Image Index
		* No Index
		* No Snippet
		* No Translate
		* Snippet Max. Length
		* Image Preview Size
		* Video Max. Previews
	* Added new options under the SSO &gt; General Settings &gt; Google tab:
		* Robots Snippet Max. Length
		* Robots Image Preview Size
		* Robots Video Max. Previews
* **Improvements**
	* Added a dismissible notice if the "meta name robots" HTML tag and a known SEO plugin has not been detected.
	* Removed the "Robots" options from the classic editor Publish metabox.
	* Replaced the option tooltip help image with a font.
* **Bugfixes**
	* Fixed an important bug, introduced in v8.3.0, that prevented getting archive post objects when not in an archive page.
	* Fixed the admin toolbar overlaying the block editor in fullscreen mode when there are SSO notifications.
* **Developer Notes**
	* Refactored the `WpssoSchema::get_page_posts_mods()` method to allow getting archive post objects when not in an archive page.
	* Refactored `SucomUtil::get_robots_default_directives()` to include disabled directives as well.
	* Refactored `WpssoUtil::get_robots_content()` to use custom options from the Document SSO metabox.
	* Added a post/term/user ID argument to the `WpssoWpMeta->upgrade_options()` method.
	* Added new `get_meta()`, `update_meta()`, and `delete_meta()` static methods in the WpssoWpMeta, WpssoPost, WpssoTerm, and WpssoUser classes.
	* Moved the `get_attached()`, `add_attached()`, and `delete_attached()` static methods from the WpssoPost, WpssoTerm, and WpssoUser classes to the WpssoWpMeta class.
	* Changed `WpssoWpMeta->must_be_extended()` to a `WpssoWpMeta::must_be_extended()` static method.
	* Removed the `WpssoPost->show_robots_options()` and `WpssoPost->save_robots_options()` methods (replaced by the "Robots Meta" tab in the Document SSO metabox).
	* Added custom CSS filters for Yoast SEO and Rank Math in the WpssoCompat class.
* **Requires At Least**
	* PHP v5.6.
	* WordPress v4.2.

**Version 8.3.1 (2020/09/06)**

* **New Features**
	* None.
* **Improvements**
	* Improved the CSS of plugin settings pages.
	* Improved the notice reference message when adding Schema Organization, Person, and Place markup.
	* Updated the SSO menu and toolbar notification icons.
* **Bugfixes**
	* None.
* **Developer Notes**
	* Added `SucomNotice->is_enabled()`, `SucomNotice->enable()`, and `SucomNotice->disable()` methods for WPSSO REST v2.6.1.
* **Requires At Least**
	* PHP v5.6.
	* WordPress v4.2.

**Version 8.3.0 (2020/09/05)**

* **New Features**
	* None.
* **Improvements**
	* Updated the cleanup method for Rank Math to remove only the Facebook and Twitter meta tags.
* **Bugfixes**
	* Fixed the Schema markup user profile query to include only posts and not pages.
* **Developer Notes**
	* Added a new `SucomUtil::get_wp_url()` method.
	* Refactored the `SucomUtil::is_toplevel_edit()` method.
	* Refactored the `WpssoUtil->get_page_url()` method to handle 'is_search' and 'is_date' module array elements.
	* Refactored the `WpssoPage->get_description()` method to handle 'is_search' and 'is_date' module array elements.
	* Refactored the `WpssoPage->get_the_title()` method to handle 'is_search' and 'is_date' module array elements.
	* Added a lib/compat.php library file for third-party plugin and theme compatibility actions and filters.
* **Requires At Least**
	* PHP v5.6.
	* WordPress v4.2.

**Version 8.2.2 (2020/08/28)**

* **New Features**
	* None.
* **Improvements**
	* None.
* **Bugfixes**
	* Fixed the Shopper Approved integration modules to skip the WooCommerce shop archive page (Premium version).
* **Developer Notes**
	* None.
* **Requires At Least**
	* PHP v5.6.
	* WordPress v4.2.

**Version 8.2.1 (2020/08/15)**

**Google has updated their Rich Results requirements to use the complete URL of Schema enumeration values instead of only the enumeration name (as they previously required). The product availability, product condition, event attendance, event status, and offer availability values have all been updated to include their complete enumeration URL. For example, a previous product condition might have been 'New' or 'NewCondition' and will now be included in Schema markup as 'https://schema.org/NewCondition'.**

* **New Features**
	* None.
* **Improvements**
	* Added 'https://schema.org/' to the Schema item availability, item condition, event attendance, and event status property values.
* **Bugfixes**
	* Fixed a possible missing 'publisher' property for the Schema Article type.
* **Developer Notes**
	* Added a new 'wpsso_sanitize_md_options' filter hook in WpssoSchema to sanitize the post metadata product availability and condition values.
	* Added a new `WpssoSchema::check_prop_value_enumeration()` method.
	* Deprecated the `WpssoSchema::check_itemprop_content_map()` method.
	* Replaced the jQuery `.load( handler )` event function trigger with `.on( 'load', handler )`.
	* Replaced the jQuery `hover` event with `mouseenter` in SucomForm->get_event_load_json_script().
* **Requires At Least**
	* PHP v5.6.
	* WordPress v4.2.

**Version 8.1.0 (2020/08/13)**

* **New Features**
	* None.
* **Improvements**
	* Added new options under SSO &gt; Advanced Settings &gt; Shopper Approved (Premium version):
		* Maximum Number of Reviews (100)
		* Maximum Age of Reviews (60) months
* **Bugfixes**
	* Fixed a PHP fatal error for `SucomUtil::preg_grep_keys()` when adding a custom video from the Document SSO metabox.
* **Developer Notes**
	* Added extra checks and debugging messages for a possible invalid video array from the `WpssoOpenGraph->get_all_videos()` method.
* **Requires At Least**
	* PHP v5.6.
	* WordPress v4.2.

**Version 8.0.0 (2020/08/11)**

**Google has updated their Rich Results requirements and now prefers 1:1, 4:3, and 16:9 images for all Schema types, not just the Schema Article type for AMP webpages. The "Schema" and "Schema Article" image sizes have been removed and replaced by new Schema 1:1, 4:3, and 16:9 image sizes (minimum dimensions are 1200x1200px, 1200x900px, and 1200x675px).**

**The [WP eCommerce](https://wordpress.org/plugins/wp-e-commerce/) plugin is no longer supported (last update in January 2019) and the WPSSO Core Premium integration module for WP eCommerce has been removed.**

* **New Features**
	* Added an API integration module for Shopper Approved ratings and reviews. See the SSO &gt; Advanced Settings &gt; Service APIs tab to enter your Shopper Approved API token and enabled reviews for specific post types (Premium version).
* **Improvements**
	* Added a new "Gravatar Image Size" option (default is 1200px).
	* Added shortening service API status to the SSO &gt; Dashboard &gt; Premium Features Status metabox:
		* Bitly Shortener API
		* DLMY.App Shortener API
		* Ow.ly Shortener API
		* TinyURL Shortener API
		* YOURLS Shortener API
	* Renamed the following SSO &gt; Image Sizes:
		* Schema Article AMP 1:1 to Schema 1:1 (Google)
		* Schema Article AMP 4:3 to Schema 4:3 (Google)
		* Schema Article AMP 16:9 to Schema 16:9 (Google)
	* Removed the following SSO &gt; Image Sizes:
		* Schema
		* Schema Article
	* Removed the "Schema Image URL" option in the Document SSO metabox.
	* Removed the WPSSO Core Premium integration module for the [WP eCommerce](https://wordpress.org/plugins/wp-e-commerce/) plugin (Premium version).
	* Removed the SSO &gt; Advanced Settings &gt; Cache &gt; Clear All Caches on Save Settings option (Premium version).
* **Bugfixes**
	* Fixed incorrect sanitation of plugin settings for multisite blogs.
* **Developer Notes**
	* Added a new `WpssoUtilMetabox` class.
	* Added a new `WpssoProUtilShorten->load_lib()` method.
	* Added new `WpssoSchema::is_valid_key()` and `is_valid_val()` methods.
	* Renamed the `WpssoProUtilShorten->set_instance()` method to `get_svc_instance()`.
	* Refactored `WpssoProUtilShorten->get_svc_instance()` to use the new `self->load_lib()` method.
	* Added more debugging messages to the `WpssoProEcomWooCommerce` class for missing product class methods (Premium version).
	* Added a 'label_transl' key to the `$features` array for the SSO &gt; Dashboard &gt; Premium Features Status metabox.
	* Refactored the Gravatar integration module to always fallback to the "mystery person" image.
	* Refactored the `WpssoMedia->get_featured()` method to use a local cache to avoid duplicate database queries.
	* Refactored the `WpssoMedia->get_attached_images()` method to use a local cache to avoid duplicate database queries.
* **Requires At Least**
	* PHP v5.6.
	* WordPress v4.2.

== Upgrade Notice ==

= 8.34.0-b.1 =

(2021/06/29) Head markup cache is now disabled if a caching plugin or service is detected.

= 8.33.0 =

(2021/06/27) Added a new module for Stamped.io ratings and reviews in the WPSSO Core Premium plugin.

= 8.32.0 =

(2021/06/23) Added a new SSO &gt; Advanced Settings &gt; Caching &gt; Cache Attachment Markup option. Fix to remove the Document SSO metabox from the WooCommerce orders and coupons editing pages.

= 8.31.0 =

(2021/06/18) Added a snackbar reminder in the block editor in case there are any important error messages under the SSO notification icon. Added a new SSO &gt; Advanced Settings &gt; Caching &gt; Cache Date Archive Markup option.

= 8.30.2 =

(2021/06/16) Fixed a non-working and duplicated copy-to-clipboard icon issue. Moved the SSO &gt; Features metaboxes to the SSO &gt; Dashboard page.

= 8.30.1 =

(2021/06/12) Fixed the missing text length message for textarea fields in the Document SSO metabox.

= 8.30.0 =

(2021/06/08) Added a thumbnail to all Image ID options. Updated the YouTube video API URL query arguments (Premium version).

= 8.29.0 =

(2021/05/30) Removed the deprecated WP Ultimate Recipe integration module (Premium version). Updated the WP Recipe Maker integration module to include instruction sections and images (Premium version).

= 8.28.2 =

(2021/05/15) Added cache refresh when a WooCommerce product changes on the front-end - for example, from in stock to out of stock (Premium version).

= 8.28.1 =

(2021/05/05) Fixed the missing jQuery sucomTextLen() function calls to display text limits after saving/updating in the block editor.

= 8.28.0 =

(2021/04/30) Added a new SSO &gt; Advanced Settings &gt; WordPress Sitemaps metabox with options to customize the post and taxonomy types included in the WordPress sitemap XML.

= 8.27.0 =

(2021/04/24) Removed support for the rtMedia plugin (Premium version). Refactored the Yoast SEO integration module (Premium version). Fixed merging of new plugin / add-on options keys during update.

= 8.26.3 =

(2021/04/17) Included support for custom post types that do not appear in the WordPress menu. Allowed 'expired' posts to have an 'article:published_time' meta tag.

= 8.26.2 =

(2021/04/13) Added a new SSO &gt; Tools and Actions &gt; Clear Failed URL Connections button.

= 8.26.1 =

(2021/04/02) Moved the SSO &gt; Image Sizes and SSO &gt; Document Types settings to the SSO &gt; Advanced Settings page.

= 8.25.2 =

(2021/03/30) Added a 'wpsso_add_schema_head_attributes' filter check (true by default) before validating theme header templates.

= 8.25.1 =

(2021/03/19) Added an `is_embed()` check for the robots 'noindex' default.

= 8.25.0 =

(2021/03/11) Added new options in the Document SSO metabox and settings pages. Fixed plugin conflict detection with All In One SEO v4.0.

= 8.24.0 =

(2021/03/04) Added theme templates for iframe embed content with additional support for custom image URLs.

= 8.23.0 =

(2021/02/25) Added new business page URL and custom contact options. Updated the banners and icons of WPSSO Core and its add-ons.

= 8.22.0 =

(2021/02/09) Updated product offer methods in `WpssoSchema` for WPSSO JSON v4.14.0.

= 8.21.0 =

(2021/01/30) Added new functions to retrieve Facebook / Open Graph image URLs. Refactored error handling and error messages in URL shortening classes (Premium version). Removed the 'wpsso_version_updates' action.

= 8.20.0 =

(2021/01/21) Added support for images in WooCommerce product reviews (Premium version). Fixed jQuery "document ready" event incompatibility with the block editor.

= 8.19.4 =

(2021/01/17) Fixed relocation of toolbar notices in WooCommerce 4.9.0 product pages.

= 8.19.3 =

(2021/01/16) Updated the jQuery event to update toolbar notices.

= 8.19.2 =

(2021/01/11) Updated `jQuery( document ).ready()` calls to `jQuery( document ).on( 'ready' )`.

= 8.19.1 =

(2021/01/07) Fix to allow the new "Disable Cache for Debugging" option to be changed in the Standard version.

= 8.19.0 =

(2020/12/29) Added a new "Disable Cache for Debugging" option in the SSO &gt; Advanced Settings page. Fixed a missing 'article:modified_time' meta tag value.

= 8.18.0 =

(2020/12/17) Updated product information text in the Document SSO metabox for WooCommerce (Premium version).

= 8.17.1 =

(2020/12/13) Fixed an incorrect 'wpsso_post_url' argument count in the NextGEN Gallery integration module (Premium version).

= 8.17.0 =

(2020/12/11) Fixed canonical and sharing URL pagination. Fixed Schema SearchResultsPage markup to use the search query.

= 8.16.0 =

(2020/12/08) Added a new "Primary Category" option in the Document SSO metabox.

= 8.15.0 =

(2020/12/05) Added a new SSO &gt; Advanced Settings &gt; Services API metabox.

= 8.14.1 =

(2020/11/30) Improved suggested add-on notifications for the WooCommerce plugin. Fixed an "undeclared static property: SucomUtilRobots::$directives" error.

= 8.14.0 =

(2020/11/26) Added WP Sitemaps filters to add article modified dates, and exclude posts / pages, terms, and users with 'noindex' checked under the Document SSO &gt; Robots Meta tab.

= 8.13.0 =

(2020/11/20) This release includes a new "Validators" toolbar menu and several changes to the Advanced Settings page.

= 8.12.1 =

(2020/11/13) This release adds a new SSO &gt; Features Status page and integration module for SEOPress (Premium version).

= 8.11.2 =

(2020/11/07) Fixed an undefined index streamingData error in cases where YouTube video details are incomplete or could not be retrieved.

= 8.11.1 =

(2020/10/31) Fixed incorrect array type casting when retrieving custom event or job options.

= 8.11.0 =

(2020/10/29) Added extra article and product Twitter Card meta tags for Slack. Fixed formatting of the Schema 'cutoffTime', 'opens', and 'closes' property values.

= 8.10.0 =

(2020/10/28) Added 'businessDays' and 'cutoffTime' to Schema ShippingDeliveryTime markup for the WPSSO Shipping Delivery Time for WooCommerce add-on.

= 8.9.0 =

(2020/10/24) Updated the markdown library used to parse WordPress readme files.

= 8.8.1 =

(2020/10/17) Added a new WPSSO Shipping Delivery Time for WooCommerce add-on.

= 8.7.1 =

(2020/10/04) Added a check for WooCommerce shipping requirements when getting shipping rates. Changed the Schema OfferShippingDetails shippingRate property from min/max values to an array of values.

= 8.7.0 =

(2020/10/02) Fixed a bug that prevented abbreviated Schema enumerations from being detected. Added support for the new Schema shippingDetails property in Schema Product offers for WooCommerce products.

