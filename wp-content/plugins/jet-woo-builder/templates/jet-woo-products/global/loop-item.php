<?php
/**
 * Products loop item template
 */

global $product;

$product             = wc_get_product( $product->get_id() );
$product_id          = $product->get_id();
$classes             = [ 'jet-woo-builder-product' ];

if ( $enable_thumb_effect ) {
	array_push( $classes, 'jet-woo-thumb-with-effect' );
}

if ( $carousel_enabled ) {
	array_push( $classes, 'swiper-slide' );
}

if ( $clickable_item ) {
	array_push( $classes, 'jet-woo-item-overlay-wrap' );
}

if ( $settings['carousel_direction'] !== 'vertical' ) {
	array_push( $classes, jet_woo_builder_tools()->col_classes(
		[
			'desk' => $this->get_attr( 'columns' ),
			'tab'  => $this->get_attr( 'columns_tablet' ),
			'mob'  => $this->get_attr( 'columns_mobile' ),
		]
	) );
}
?>

<div class="jet-woo-products__item <?php echo implode( ' ', $classes ); ?>" data-product-id="<?php echo $product_id ?>">
	<div class="jet-woo-products__inner-box"><?php include $this->get_product_preset_template(); ?></div>
	<?php if ( $clickable_item ) : ?>
		<a href="<?php echo esc_url( get_permalink() ); ?>" class="jet-woo--item-overlay-link" <?php echo $target_attr; ?>></a>
	<?php endif; ?>
</div>