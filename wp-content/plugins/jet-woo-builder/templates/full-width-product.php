<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
get_header( 'shop' );

do_action( 'jet-woo-builder/full-width-page/before-content' );

while ( have_posts() ) :
	the_post();

	if ( ! empty( $_GET['elementor-preview'] ) ) {
		the_content();
	} else {
		wc_get_template_part( 'content', 'single-product' );
	}
endwhile;

do_action( 'jet-woo-builder/full-width-page/after_content' );

get_footer( 'shop' );