<?php

if (!defined('ABSPATH')) {
    exit;
} // Exit if accessed directly

if (!class_exists('CSF_Field_url_types')) {
    class CSF_Field_url_types extends \CSF_Fields
    {
        public function __construct($field, $value = '', $unique = '', $where = '', $parent = '')
        {
            parent::__construct($field, $value, $unique, $where, $parent);
        }

        public function render()
        {
            echo $this->field_before();

            echo $this->get_url_types_field_content();

            echo $this->field_after();
        }

        protected function get_url_types_field_content()
        {
            $args = wp_parse_args($this->field, array(
                'inline' => false,
                'query_args' => array(),
            ));
            $html = '';
            $inline_class = ($args['inline']) ? ' class="csf--inline-list"' : '';

            $is_premium = hf_fs()->is__premium_only() ? false : false;

            if (isset($this->field['options'])) {

                $options = $this->field['options'];
                $options = (is_array($options)) ? $options : array_filter($this->field_data($options, false, $args['query_args']));

                if (is_array($options) && !empty($options)) {

                    $html .= '<div' . $inline_class . '>';
                    foreach ($options as $option_key => $option_value) {

                        $checked = ($option_key == $this->value) ? ' checked' : '';
                        $disabled_class = (!$is_premium && $option_key == 'temp_post_slug') ? 'helpie-disabled' : '';

                        $html .= '<div class="' . $disabled_class . '"><label><input type="radio" name="' . esc_attr($this->field_name()) . '" value="' . esc_attr($option_key) . '"' . $this->field_attributes() . esc_attr($checked) . (!empty($disabled_class) ? 'disabled' : '') . '/> ' . esc_attr($option_value) . '</label></div>';
                    }
                    $html .= '</div>';

                } else {

                    $html .= (!empty($this->field['empty_message'])) ? esc_attr($this->field['empty_message']) : esc_html__('No data provided for this option type.', 'csf');

                }

            } else {
                $label = (isset($this->field['label'])) ? $this->field['label'] : '';
                $html .= '<label><input type="radio" name="' . esc_attr($this->field_name()) . '" value="1"' . $this->field_attributes() . esc_attr(checked($this->value, 1, false)) . '/> ' . esc_attr($label) . '</label>';
            }

            return $html;
        }
    }
}