/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "./";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./assets/js/main.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./assets/css/main.scss":
/*!******************************!*\
  !*** ./assets/css/main.scss ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./assets/js/components/insights/tracker.js":
/*!**************************************************!*\
  !*** ./assets/js/components/insights/tracker.js ***!
  \**************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/* Triggered by FAQ events, integrates with Insights */
var Tracker = {
  init: function init() {
    this.nonce = helpie_faq_object.nonce; // this.eventHandler();
  },

  /* EVENTS API */
  searchCounter: function searchCounter(searchTerm) {
    var thisModule = this;
    var data = {
      action: "helpie_faq_search_counter",
      nonce: thisModule.nonce,
      searchTerm: searchTerm
    };
    jQuery.post(helpie_faq_object.ajax_url, data, function (response) {// console.log(response);
    });
  },

  /* Auto-ordering methods */
  clickCounter: function clickCounter(id) {
    this.ajaxRequest(id);
  },

  /* INTERNAL METHODS */
  ajaxRequest: function ajaxRequest(id) {
    var thisModule = this;
    var data = {
      action: "helpie_faq_click_counter",
      nonce: thisModule.nonce,
      id: id
    };
    jQuery.post(helpie_faq_object.ajax_url, data, function (response) {// console.log(response);
    });
  }
};
module.exports = Tracker;

/***/ }),

/***/ "./assets/js/components/submission/submission.js":
/*!*******************************************************!*\
  !*** ./assets/js/components/submission/submission.js ***!
  \*******************************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var SubmissionAjax = __webpack_require__(/*! ./submission_ajax.js */ "./assets/js/components/submission/submission_ajax.js");

var Submission = {
  init: function init() {
    var enabled_submission = helpie_faq_object.enabled_submission;

    if (enabled_submission == "") {
      return false;
    }

    this.eventHandler();
  },
  eventHandler: function eventHandler() {
    this.initChosen();
    this.toggleForm();
    this.submitForm();

    if (helpie_faq_object.plan == "premium") {
      SubmissionAjax.getLoggedEmail();
    }
  },
  initChosen: function initChosen() {
    jQuery(".helpie-faq").find(".form__section .faq_categories").each(function () {
      jQuery(this).chosen({
        width: "100%"
      });
    });
  },
  submitForm: function submitForm() {
    var thisModule = this;
    jQuery(".helpie-faq").on("click", ".form__submit", function (e) {
      e.stopPropagation();
      var formSection = jQuery(this).closest(".form__section"),
          question = formSection.find(".form__text").val(),
          email = formSection.find(".form__email").val(),
          answer = formSection.find(".form__textarea").val(),
          wooProduct = formSection.data("woo-product"),
          kbCategory = formSection.data("kb-category"),
          categories = formSection.find(".faq_categories").val(),
          faqGroupID = formSection.find("input[name=faq_group_id]").val(),
          data = {
        action: "helpie_faq_submission",
        nonce: thisModule.nonce,
        question: question,
        categories: categories,
        group_id: faqGroupID
      };
      if (email) data.email = email;
      if (answer) data.answer = answer;
      if (wooProduct) data.woo_product = wooProduct;
      if (kbCategory) data.kb_category = kbCategory;

      if (question && thisModule._isEmail(email) || question && email == undefined) {
        e.preventDefault();
        SubmissionAjax.postForm(data, formSection);
      }
    });
  },
  toggleForm: function toggleForm() {
    jQuery(".helpie-faq").on("click", ".form__toggle", function (e) {
      e.preventDefault();
      e.stopPropagation();
      var formSection = jQuery(this).parent().next(".form__section");
      var currentToggleButtonValue = this.value;
      var toggleButtonValue = "";
      formSection.next().hide();

      if (currentToggleButtonValue === faqStrings.addFAQ) {
        toggleButtonValue = faqStrings.hide;
        formSection.show();
      } else {
        toggleButtonValue = faqStrings.addFAQ;
        formSection.hide();
      }

      this.value = toggleButtonValue;
    });
  },
  _isEmail: function _isEmail(email) {
    var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return !regex.test(email) ? false : true;
  }
};
module.exports = Submission;

/***/ }),

/***/ "./assets/js/components/submission/submission_ajax.js":
/*!************************************************************!*\
  !*** ./assets/js/components/submission/submission_ajax.js ***!
  \************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var SubmissionAjax = {
  postForm: function postForm(data, formSection) {
    thisModule = this;
    jQuery.post(helpie_faq_object.ajax_url, data, function (response) {
      var ajaxResponse = JSON.parse(response); // console.log(ajaxResponse);

      if (ajaxResponse.postStatus == "publish") {
        thisModule._successMessage(formSection);
        /*** Reload the page insteead of appending the content */
        // thisModule._appendItem(data, formSection);


        setTimeout(function () {
          location.reload();
        }, 1000); // console.log("Success Post Message");
      } else if (ajaxResponse.postStatus == "pending") {
        thisModule._successMessage(formSection); // console.log("Success Pending Message");

      }
    });
    var faqDefaultCategoryId = formSection.find(".faq_default_category_term_id").val(); // Empty input fields on submit

    formSection.find(".form__text").val("");
    formSection.find(".form__email").val("");
    formSection.find(".form__textarea").val("");
    formSection.find(".faq_categories").val(faqDefaultCategoryId).trigger("chosen:updated");

    if (helpie_faq_object.plan == "premium") {
      thisModule.getLoggedEmail();
    }
  },
  getLoggedEmail: function getLoggedEmail() {
    thisModule = this;
    var data = {
      action: "helpie_faq_submission_get_logged_email",
      nonce: thisModule.nonce
    };
    var email = jQuery(".form__section .form__email");

    if (email) {
      jQuery.get(helpie_faq_object.ajax_url, data, function (response) {
        var ajaxResponse = JSON.parse(response);

        if (ajaxResponse.loggedEmail) {
          email.val(ajaxResponse.loggedEmail);
        }
      });
    }
  },
  _appendItem: function _appendItem(data, formSection) {
    var accordion = formSection.parent().find(".accordion");
    var reqData = {
      action: "helpie_faq_submission_get_item",
      nonce: thisModule.nonce,
      title: data.question
    };
    reqData.content = data.answer ? data.answer : "Empty Content";
    jQuery.post(helpie_faq_object.ajax_url, reqData, function (response) {
      var ajaxResponse = JSON.parse(response);
      accordion.append(ajaxResponse.singleItem); // console.log(ajaxResponse);
    });
  },
  _successMessage: function _successMessage(el) {
    el.hide();
    el.next().show();
  }
};
module.exports = SubmissionAjax;

/***/ }),

/***/ "./assets/js/main.js":
/*!***************************!*\
  !*** ./assets/js/main.js ***!
  \***************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _css_main_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../css/main.scss */ "./assets/css/main.scss");
/* harmony import */ var _css_main_scss__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_css_main_scss__WEBPACK_IMPORTED_MODULE_0__);
var Stylus = __webpack_require__(/*! ./../../lib/stylus/js/search.js */ "./lib/stylus/js/search.js");

var Tracker = __webpack_require__(/*! ./components/insights/tracker.js */ "./assets/js/components/insights/tracker.js");

var Submission = __webpack_require__(/*! ./components/submission/submission.js */ "./assets/js/components/submission/submission.js");

var HelpieFaq = {
  nonce: helpie_faq_object.nonce,
  init: function init() {
    // console.log("FAQ init");
    this.onPageLoadActions();
    this.eventHandlers(); // Components

    Tracker.init(this.nonce); // Library Components

    Stylus.init();
    
  },
  eventHandlers: function eventHandlers() {
    var thisModule = this;
    var helpieFaqToggle = document.querySelector("section.helpie-faq");

    if (helpieFaqToggle !== null && helpieFaqToggle !== "undefined" && helpieFaqToggle !== undefined) {
      helpieFaqToggle.addEventListener("click", this.helpieFaqToggleHandler, true);
    }

    jQuery(".accordion__header").on("click", function (e) {
      e.preventDefault();
      e.stopPropagation();
      var faq_list = thisModule.isFaqList(jQuery(this));

      if (faq_list) {
        return false;
      }

      thisModule.onHeaderClick(this);
      var dataItem = jQuery(this).attr("data-item");

      if (dataItem !== undefined && dataItem !== "undefined" && dataItem !== "") {
        window.location.hash = jQuery(this).attr("data-item");
      }
    });
    jQuery(".accordion__title, .faq-title-icon").on("click", function (e) {
      var $accordion__header = jQuery(this).closest(".accordion__header");
      e.preventDefault();
      e.stopPropagation();
      var faq_list = thisModule.isFaqList(jQuery(this));

      if (faq_list) {
        return false;
      }

      thisModule.onHeaderClick($accordion__header);
      var dataItem = $accordion__header.attr("data-item");

      if (dataItem !== undefined && dataItem !== "undefined" && dataItem !== "") {
        window.location.hash = $accordion__header.attr("data-item");
      }
    }); // jQuery('.helpie-faq').on('click',function(e){
    //     if(jQuery(this).hasClass('toggle')){
    //         e.stopPropagation();
    //     }
    // });
  },
  helpieFaqToggleHandler: function helpieFaqToggleHandler(e) {
    // TODO: Need to refactor
    if (e.target.classList.contains("accordion__header") != true && e.target.classList.contains("accordion__title") != true && e.target.classList.contains("faq-title-icon") != true && e.target.classList.contains("form__toggle") != true && e.target.classList.contains("form__submit") != true && e.target.classList.contains("accordion__toggle-icons") !== true && e.target.classList.contains("chosen-search-input") !== true && e.target.classList.contains("result-selected") !== true && e.target.classList.contains("search-choice-close") !== true && e.target.classList.contains("chosen-choices") !== true) {
      e.stopPropagation();
    }
  },
  openFirstAccordion: function openFirstAccordion() {
    jQuery(".helpie-faq.accordions.open-first > .accordion:first").each(function () {
      var FirstItemHeaderSelector = ".accordion__item:first > .accordion__header";
      jQuery(this).find(FirstItemHeaderSelector).addClass("active");
      jQuery(this).find(FirstItemHeaderSelector).next(".accordion__body").stop().slideDown(300);
    });
  },
  onPageLoadActions: function onPageLoadActions() {
    var urlHashTagHFaqItem = window.location.hash;
    var isHFAQAccordionOpened = false;

    if (urlHashTagHFaqItem != "") {
      isHFAQAccordionOpened = this.openHFaqAccordion(urlHashTagHFaqItem);
    }

    if (!isHFAQAccordionOpened) {
      this.openFirstAccordion();
    }
  },
  onHeaderClick: function onHeaderClick(that) {
    var thisModule = this;

    if (jQuery(that).hasClass("active")) {
      thisModule.closeAccordion(that);
    } else {
      if (jQuery(that).closest(".helpie-faq.accordions").hasClass("toggle")) {
        jQuery(that).closest(".accordion").find(".accordion__header").removeClass("active");
        jQuery(that).closest(".accordion").find(".accordion__body").slideUp();
      }

      thisModule.openAccordion(that);
    }
  },
  openAccordion: function openAccordion(that) {
    jQuery(that).addClass("active");
    jQuery(that).next(".accordion__body").stop().slideDown(300);
    /* Auto-ordering Event Tracker */

    var id = jQuery(that).attr("data-id");

    if (id) {
      Tracker.clickCounter(id);
    }
  },
  closeAccordion: function closeAccordion(that) {
    jQuery(that).removeClass("active");
    jQuery(that).next(".accordion__body").stop().slideUp(300);
  },
  openHFaqAccordion: function openHFaqAccordion(urlHashTagHFaqItem) {
    var isItemFound = false;
    jQuery(".helpie-faq.accordions .accordion .accordion__item").each(function () {
      var HFaqItem = "#" + jQuery(this).find(".accordion__header").attr("data-item");

      if (HFaqItem === urlHashTagHFaqItem) {
        jQuery(this).find(".accordion__header").addClass("active");
        jQuery(this).find(".accordion__header").next(".accordion__body").stop().slideDown(300);
        var accordions = jQuery(this).parents(".accordion");

        if (accordions.length > 0) {
          // In display mode is category then, if need to active on category faq accordion also.
          var categoryAccordion = jQuery(this).closest(".accordion").parent();
          categoryAccordion.prev().addClass("active");
          categoryAccordion.show();
        }

        var accordionHeaderPosition = jQuery(this).find(".accordion__header").offset().top;
        accordionHeaderPosition = accordionHeaderPosition - parseInt(70);
        window.scrollTo({
          top: accordionHeaderPosition,
          behavior: "smooth"
        });
        isItemFound = true;
        return false;
      }
    });
    return isItemFound;
  },
  isFaqList: function isFaqList(that) {
    var faqListLength = jQuery(that).closest("article.faq_list").length;
    return faqListLength;
  }
};
jQuery(document).ready(function () {
  HelpieFaq.init();
  console.log("jquery");
});


/***/ }),

/***/ "./lib/stylus/js/search.js":
/*!*********************************!*\
  !*** ./lib/stylus/js/search.js ***!
  \*********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var Tracker = __webpack_require__(/*! ../../../assets/js/components/insights/tracker */ "./assets/js/components/insights/tracker.js");

var selectors = {
  accordions: ".helpie-faq.accordions",
  faqSearch: ".helpie-faq .search__wrapper .search__input",
  accordion: ".accordion",
  accordionShow: "accordion--show",
  accordionHide: "accordion--hide",
  accordionHeading: ".accordion__heading",
  accordionHeadingShow: "accordion__heading--show",
  accordionHeadingHide: "accordion__heading--hide",
  accordionHeader: ".accordion .accordion__item .accordion__header",
  accordionItem: ".accordion__item",
  accordionItemShow: "accordion__item--show",
  accordionItemHide: "accordion__item--hide",
  accordionBody: ".accordion__body",
  searchMessage: ".search .search__message",
  searchMessageContent: "<p class='search__message__content'>" + faqStrings.noFaqsFound + "</p>"
};
var Stylus = {
  //setup before functions
  searchTerm: "",
  typingTimer: 0,
  // timer identifier
  doneTypingInterval: 2000,
  // time in ms, 2 second for example
  setSearchAttr: function setSearchAttr() {
    /* Add 'search_attr' to accordion headers */
    jQuery(selectors.accordionHeader).each(function () {
      var searchAttr = jQuery(this).text().toLowerCase();
      jQuery(this).attr("data-search-term", searchAttr);
    });
  },
  isContentMatch: function isContentMatch(element, searchTerm) {
    var content = jQuery(element).find(selectors.accordionBody).text().toLowerCase();
    if (content.indexOf(searchTerm) >= 0) return true;
    return false;
  },
  isTitleMatch: function isTitleMatch(element, searchTerm) {
    var content = jQuery(element).find(".accordion__header").attr("data-search-term");

    if (content == undefined || content == "undefined") {
      return false;
    }

    if (content.search(searchTerm) >= 0) {
      return true;
    }

    return false;
  },
  isCategoryHeadingMatch: function isCategoryHeadingMatch(element, searchTerm) {
    var content = jQuery(element).prev(selectors.accordionHeading).text().toLowerCase();

    if (content.indexOf(searchTerm) >= 0) {
      return true;
    }

    return false;
  },
  isCategroryAccordionMatch: function isCategroryAccordionMatch(element, searchTerm) {
    if (jQuery(element).hasClass("accordion__category") == false) {
      return false;
    }

    var thisModule = this;
    return thisModule.isTitleMatch(element, searchTerm);
  },
  searchByAccordionItem: function searchByAccordionItem(element, props) {
    var thisModule = this;
    searchTerm = thisModule.searchTerm;
    var titleMatch = thisModule.isTitleMatch(element, searchTerm);
    var contentMatch = thisModule.isContentMatch(element, searchTerm); //tags

    var tagsMatch = thisModule.searchByTags(element, searchTerm);
    var show = titleMatch || contentMatch || tagsMatch ? true : false;
    thisModule.displayAccordionItem(element, show);
    return show;
  },
  onSearchKeyup: function onSearchKeyup(that) {
    var thisModule = this;
    var searchTerm = thisModule.searchTerm;
    console.log("*** onSearchKeyup() ***");
    jQuery(that).closest(selectors.accordions).children(selectors.accordion).each(function () {
      var $accordion = jQuery(this);
      var showThatAccordion = false;
      var categoryHeadingMatch = thisModule.isCategoryHeadingMatch($accordion, searchTerm);

      if (categoryHeadingMatch == true) {
        showThatAccordion = true;
        thisModule.showAccordionSection($accordion, showThatAccordion);
      } else {
        $accordion.children(selectors.accordionItem).each(function () {
          $accordionItem = jQuery(this);
          var hasCategory = $accordionItem.hasClass("accordion__category");
          console.log(" [hasCategory]: " + hasCategory); // check the current accordion has a accordion__category class and match that accordion

          var categoryMatched = thisModule.searchByCategory($accordionItem, searchTerm); // let define a variable for use of show the current category with accordions, when hasCategory and categoryMatched variables are matched.

          var showCategoryWithAccordions = hasCategory && categoryMatched ? true : false; // let define a varible for want to search category inner accordions, if showCategoryWithAccordions var value as false

          var searchCategoryInnerAccordions = showCategoryWithAccordions == false ? true : false; // searching category inner accordions

          var innerAccordionsContentMatched = false;

          if (searchCategoryInnerAccordions) {
            innerAccordionsContentMatched = thisModule.searchInnerAccordionsItems($accordionItem, searchTerm);
          } // let define a varible for searching normal accordion, if FAQ display mode as simple accordion


          var searchSingleAccordionItem = hasCategory == false ? true : false;
          var accordionMatched = false; // find & show the accordion item

          if (searchSingleAccordionItem) {
            accordionMatched = thisModule.searchByAccordionItem($accordionItem, {});
          } // show the category with accordions


          if (hasCategory && innerAccordionsContentMatched == false) {
            thisModule.showCategoryWithAccordions($accordionItem, showCategoryWithAccordions);
          } // show the accordion with closest category accordion


          if (searchCategoryInnerAccordions && innerAccordionsContentMatched) {
            thisModule.showAccordionBelongsToCategory($accordionItem, true);
          }

          if (showThatAccordion == false) {
            // show the closest accordion, if anyone category accordion text is matched.
            if (showCategoryWithAccordions == true) {
              showThatAccordion = true;
            } // show the closest accordion, if category inner accordions items title, content, or tags are matched


            if (searchCategoryInnerAccordions == true && innerAccordionsContentMatched == true) {
              showThatAccordion = true;
            } // show the accordion, if simple accordion matched.


            if (searchSingleAccordionItem == true && accordionMatched == true) {
              showThatAccordion = true;
            }
          }
        });
      }

      thisModule.displayHeading($accordion, showThatAccordion);
      thisModule.showAccordion($accordion, showThatAccordion);
    });
  },
  searchByCategory: function searchByCategory($categoryAccordion, searchTerm) {
    var thisModule = this;
    var categoryAccordionMatched = thisModule.isCategroryAccordionMatch($categoryAccordion, searchTerm);
    return categoryAccordionMatched;
  },
  searchInnerAccordionsItems: function searchInnerAccordionsItems($categoryAccordion, searchTerm) {
    var thisModule = this;
    var show = false; // loop the current accordion and get inner accordions items.

    $categoryAccordion.find(selectors.accordionItem).each(function () {
      $item = jQuery(this);
      var itemMatched = thisModule.searchByAccordionItem($item, {});

      if (itemMatched == true) {
        show = true;
      }
    });
    return show;
  },
  init: function init() {
    var thisModule = this;
    console.log("Search init");
    thisModule.setSearchAttr();
    jQuery("body").on("keyup", selectors.faqSearch, function (event) {
      var searchValue = jQuery(this).val().toLowerCase().replace(/[.*+?^${}()|[\]\\]/gi, "");
      thisModule.searchTerm = searchValue; // don't show the empty faqs block, if users entering the keywords for filtering faqs

      thisModule.canSeeEmptyFAQsBlock(this, "hide");

      if (thisModule.searchTerm == "") {
        // show all faqs, if search term has an empty.
        thisModule.showAllAccordions(this);
        return false;
      }

      var searchTerm = thisModule.onSearchKeyup(this); // shown not found faq's content

      thisModule.showEmptyFAQsContent(this); // use of condition for avoid to tracking empty search term.

      if (thisModule.searchTerm == "") {
        return false;
      } // For ajaxCall, start the countdown


      clearTimeout(thisModule.typingTimer);
      thisModule.typingTimer = setTimeout(function () {
        // donetyping() method
        // For Tracking, searching Keywords
        Tracker.searchCounter(searchTerm);
      }, thisModule.doneTypingInterval);
    });
  },
  showAllAccordions: function showAllAccordions(that) {
    var thisModule = this;
    var canDisplay = true;
    jQuery(that).closest(selectors.accordions).children(selectors.accordion).each(function () {
      var $accordion = jQuery(this); //show accordion

      thisModule.showAccordion($accordion, canDisplay); //show heading

      thisModule.displayHeading($accordion, canDisplay); // show inner accordion

      $accordion.find(selectors.accordion).removeClass(selectors.accordionHide).addClass(selectors.accordionShow); //show accordion items

      $accordion.find(selectors.accordionItem).removeClass(selectors.accordionItemHide).addClass(selectors.accordionItemShow);
    });
  },
  showEmptyFAQsContent: function showEmptyFAQsContent(that) {
    var thisModule = this;
    var hiddenFaqsCount = 0;
    var totalFaqsCount = jQuery(that).closest(selectors.accordions).find(selectors.accordionItem).length;
    jQuery(that).closest(selectors.accordions).find(selectors.accordionItem).each(function () {
      // count all hidden faqs
      if (jQuery(this).is(":visible") == false) {
        hiddenFaqsCount = parseInt(hiddenFaqsCount) + 1;
      }
    }); // check hidden faqs-count with overall faq-count, return if not match both counts.

    if (hiddenFaqsCount != totalFaqsCount) {
      return;
    }

    jQuery(that).closest(selectors.accordions).find(selectors.accordion).each(function () {
      var $accordion = jQuery(this);
      thisModule.displayHeading($accordion, false);
      thisModule.showAccordion($accordion, false);
    }); // append & show the not found faq text block

    jQuery(that).closest(selectors.accordions).find(selectors.searchMessage).empty().show().append(selectors.searchMessageContent);
  },
  canSeeEmptyFAQsBlock: function canSeeEmptyFAQsBlock(that, status) {
    var canSeeElement = "none";

    if (status == "show") {
      canSeeElement = "block";
    }

    jQuery(that).closest(selectors.accordions).find(selectors.searchMessage).css("display", canSeeElement);
  },
  displayAccordionItem: function displayAccordionItem(event, canDisplay) {
    var addClassName = canDisplay == true ? selectors.accordionItemShow : selectors.accordionItemHide;
    var removeClassName = canDisplay == false ? selectors.accordionItemShow : selectors.accordionItemHide;
    event.removeClass(removeClassName).addClass(addClassName);
  },
  displayHeading: function displayHeading(event, canDisplay) {
    var addClassName = canDisplay == true ? selectors.accordionHeadingShow : selectors.accordionHeadingHide;
    var removeClassName = canDisplay == false ? selectors.accordionHeadingShow : selectors.accordionHeadingHide;
    event.prev(selectors.accordionHeading).removeClass(removeClassName).addClass(addClassName);
  },
  showCategoryAccordions: function showCategoryAccordions(element, canDisplay) {
    var addClassName = canDisplay == true ? selectors.accordionItemShow : selectors.accordionItemHide;
    var removeClassName = canDisplay == false ? selectors.accordionItemShow : selectors.accordionItemHide;
    jQuery(element).find(selectors.accordionItem).removeClass(removeClassName).addClass(addClassName);
  },
  showAccordionSection: function showAccordionSection(element, canDisplay) {
    var thisModule = this;
    thisModule.displayHeading(element, canDisplay);
    thisModule.showCategoryAccordions(element, canDisplay);
    thisModule.showAccordion(element, canDisplay);
  },
  showAccordion: function showAccordion(element, canDisplay) {
    var addClassName = canDisplay == true ? selectors.accordionShow : selectors.accordionHide;
    var removeClassName = canDisplay == false ? selectors.accordionShow : selectors.accordionHide;
    jQuery(element).removeClass(removeClassName).addClass(addClassName);
  },
  showCategoryWithAccordions: function showCategoryWithAccordions(element, canDisplay) {
    var thisModule = this;
    thisModule.displayAccordionItem(element, canDisplay);
    thisModule.showCategoryAccordions(element, canDisplay);
    thisModule.showAccordion(element, canDisplay);
  },
  showAccordionBelongsToCategory: function showAccordionBelongsToCategory(element, canDisplay) {
    var thisModule = this;
    jQuery(element).find(selectors.accordion).removeClass(selectors.accordionHide).addClass(selectors.accordionShow);
    thisModule.displayAccordionItem(element, canDisplay);
  },
  searchByTags: function searchByTags(element, searchTerm) {
    var tags = jQuery(element).find(".accordion__header").attr("data-tags");
    var foundTag = false;

    if (tags == undefined || tags == "undefined" || tags.length == 0) {
      return foundTag;
    }

    tags.split(",").forEach(function (tag) {
      tag = tag.toLowerCase();

      if (tag.search(searchTerm) != -1) {
        foundTag = true;
      }
    });
    return foundTag;
  }
};
module.exports = Stylus;

/***/ })

/******/ });
//# sourceMappingURL=main.bundle.js.map