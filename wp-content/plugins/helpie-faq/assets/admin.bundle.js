/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "./";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./assets/js/admin.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./assets/css/admin.scss":
/*!*******************************!*\
  !*** ./assets/css/admin.scss ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./assets/js/admin.js":
/*!****************************!*\
  !*** ./assets/js/admin.js ***!
  \****************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _css_admin_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../css/admin.scss */ "./assets/css/admin.scss");
/* harmony import */ var _css_admin_scss__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_css_admin_scss__WEBPACK_IMPORTED_MODULE_0__);
// Admin Styles


var Insights = __webpack_require__(/*! ./components/insights/insights.js */ "./assets/js/components/insights/insights.js");

var FaqGroups = __webpack_require__(/*! ./components/faq/faq_groups.js */ "./assets/js/components/faq/faq_groups.js");

var StyleSettings = __webpack_require__(/*! ./settings/styles.js */ "./assets/js/settings/styles.js");

var Admin = {
  init: function init() {
    this.nonce = helpie_faq_object.nonce;
    this.eventhandlers();
    Insights.init();
    FaqGroups.init();
    StyleSettings.init();
  },
  eventhandlers: function eventhandlers() {
    var thisModule = this;
    var publish = document.getElementById("helpie_faq_delete");
    if (publish !== null) publish.onclick = function () {
      // return confirm();
      var question = "Are you sure you want to reset all Insights?";
      if (confirm(question)) thisModule.resetInsights();
    };
    jQuery(".helpie-disabled").on("click", function () {
      thisModule.showFAQPurchaseModalNotice();
    });
    jQuery(".csf--switcher").on("click", function (evt) {
      var $csf_switcher = jQuery(this).closest(".csf-field-switcher");

      if ($csf_switcher.hasClass("helpie-disabled")) {
        evt.stopImmediatePropagation();
        thisModule.showFAQPurchaseModalNotice();
        return false;
      }

      return true;
    });
  },
  resetInsights: function resetInsights() {
    var data = {
      action: "helpie_faq_reset_insights",
      nonce: this.nonce
    };
    jQuery.post(helpie_faq_object.ajax_url, data, function (response) {
      location.reload();
    });
  },
  showFAQPurchaseModalNotice: function showFAQPurchaseModalNotice() {
    jQuery.magnificPopup.open({
      items: {
        src: "#hfaq-pro-popup-notice",
        type: "inline"
      }
    });
  }
};
jQuery(document).ready(function () {
  Admin.init();
});

/***/ }),

/***/ "./assets/js/components/faq/faq_groups.js":
/*!************************************************!*\
  !*** ./assets/js/components/faq/faq_groups.js ***!
  \************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var FaqGroups = {
  init: function init() {
    console.log("*** Init Faq Groups ***");
    this.helpie_faq_group = helpie_faq_group;

    if (this.helpie_faq_group.is_page == false) {
      return false;
    }

    console.log(this.helpie_faq_group);
    this.pending_post_ids = this.helpie_faq_group.pending_post_ids;
    this.eventListeners();
  },
  eventListeners: function eventListeners() {
    var thisModule = this;
    var hfaqGroupSelector = thisModule.getSelectors();
    console.log("** hfaqGroupSelector **");
    console.log(hfaqGroupSelector);
    var isFaqGroupSummaryPage = hfaqGroupSelector.hfaq_group_taxonomy.find("form#addtag").length > 0;
    console.log("isFaqGroupSummaryPage  - " + isFaqGroupSummaryPage);

    if (isFaqGroupSummaryPage && this.helpie_faq_group.page_action === "show_faq_groups") {
      thisModule.showGroupSummaryWithLinkBtn();
    }

    if (isFaqGroupSummaryPage && this.helpie_faq_group.page_action === "create_faq_group") {
      thisModule.showFaqGroupForm();
    }

    thisModule.loadFaqGroupAccordions();
    jQuery(hfaqGroupSelector.repeater).on("click", hfaqGroupSelector.repeater_clone, function (e) {
      e.preventDefault();
      var $parentItem = jQuery(this).closest(".csf-repeater-item");
      var index = $parentItem.index() + 1;
      var $repeaterItems = jQuery(hfaqGroupSelector.repeater).find(hfaqGroupSelector.repeater_items);
      var $currentItem = $repeaterItems.eq(index);
      var $accordionContent = $currentItem.find(hfaqGroupSelector.accordion_content);
      $post = $accordionContent.find('input[name="' + thisModule.getPostFieldName(index) + '"]');

      if ($post.length > 0) {
        // Set Copied Element value is 0
        $post.attr("value", "0");
      }
    }); // shortcode copy clipboard in editing term page

    jQuery(".helpie-faq-groups-table").on("click", ".term-shortcode-wrap .clipboard-text", function () {
      var copyText = document.getElementById("faq-group-shortcode");
      copyText.select();
      copyText.setSelectionRange(0, 99999);
      /* Copy the text inside the text field */

      document.execCommand("copy");
    });
    jQuery(".hfaq-group-page").on("click", ".faq_group_shortcode .helpie-faq-group .clipboard", function () {
      // Copy Shortcode Content from faq group summary page
      var shortcodeContent = jQuery(this).parent().find(".shorcode-content").text();
      var textArea = document.createElement("textarea");
      textArea.value = shortcodeContent;
      document.body.appendChild(textArea);
      textArea.select();
      document.execCommand("Copy");
      textArea.remove();
    });
    jQuery(hfaqGroupSelector.repeater).on("click", hfaqGroupSelector.repeater_add_btn, function (e) {
      e.preventDefault(); // get latest accordion item from csf-repeater

      var $newRepeaterItem = jQuery(hfaqGroupSelector.repeater_wrapper).find(".csf-repeater-item").last();
      var $accordion = $newRepeaterItem.find(hfaqGroupSelector.accordion); // get latest accordion items from csf-repeater

      var $allAccordionItems = jQuery(hfaqGroupSelector.repeater_wrapper).find(hfaqGroupSelector.accordion_content); // Remove accordion open class

      if ($allAccordionItems.length > 0) {
        $allAccordionItems.each(function () {
          jQuery(this).removeClass("csf-accordion-open");
        });
      } //  new accordion item manually trigger to open


      var $accordionTitle = $accordion.find(".csf-accordion-item .csf-accordion-title");
      $accordionTitle.triggerHandler("click");
    });
    var fas_quora_icon = hfaqGroupSelector.fas_quora_icon; // update the accordion header title , when changing the post title content

    jQuery(hfaqGroupSelector.repeater).on("input", hfaqGroupSelector.accordion_input_title, function (event) {
      var title = jQuery(this).val();

      if (title.length == 0) {
        title = "FAQ Item";
      }

      var $accordion = jQuery(this).closest(".hfaq-groups__accordion");
      var $accordionContent = jQuery(this).closest(".csf-accordion-content");
      var postID = $accordionContent.find(".helpie-group-posts input[type='text']").val();
      var isPendingStatus = thisModule.checkPendingStatus(postID);
      var $accordionTitle = $accordion.find(".csf-accordion-title");
      var titleContent = fas_quora_icon + title;

      if (isPendingStatus == true) {
        titleContent = titleContent + hfaqGroupSelector.pending_status_notice;
      } // remove text except element inside the child elements.


      $accordionTitle.html(titleContent);
    });
  },
  showGroupSummaryWithLinkBtn: function showGroupSummaryWithLinkBtn() {
    var thisModule = this;
    var hfaqGroupSelector = thisModule.getSelectors();
    var checkPageContainer = hfaqGroupSelector.hfaq_group_taxonomy.find("#col-container").length > 0;

    if (checkPageContainer == false) {
      return false;
    } // Hide Left Column in a Page


    hfaqGroupSelector.hfaq_group_taxonomy.find("#col-left").hide(); // Set Right Column in Full Width

    hfaqGroupSelector.hfaq_group_taxonomy.find("#col-right").addClass("hfaq-group-page hfaq-full-width");
    var html = '<a href="' + this.helpie_faq_group.create_link + '" class="page-title-action hfaq-group__add--link">Add New</a>'; // jQuery(html).insertAfter(".wp-heading-inline");

    hfaqGroupSelector.hfaq_group_taxonomy.find(".wp-heading-inline").append(html);
  },
  showFaqGroupForm: function showFaqGroupForm() {
    var thisModule = this;
    var hfaqGroupSelector = thisModule.getSelectors();
    console.log(" FAQ Groups Summary");
    var checkPageContainer = hfaqGroupSelector.hfaq_group_taxonomy.find("#col-container").length > 0;

    if (checkPageContainer == false) {
      return false;
    } // Hide Left Column in a Page


    hfaqGroupSelector.hfaq_group_taxonomy.find("#col-left").addClass("hfaq-group-page hfaq-full-width"); // Set Right Column in Full Width

    hfaqGroupSelector.hfaq_group_taxonomy.find("#col-right").hide(); // Hide Taxonomy Search Form

    hfaqGroupSelector.hfaq_group_taxonomy.find(".search-form").hide();
    thisModule.setFaqgroupCreateMenuActive();
  },
  setFaqgroupCreateMenuActive: function setFaqgroupCreateMenuActive() {
    var $faqSubmenus = jQuery("#menu-posts-helpie_faq").find("ul.wp-submenu li");

    if ($faqSubmenus.length == 0) {
      return false;
    }

    $faqSubmenus.each(function () {
      var submenuText = jQuery(this).find("a").text();

      if (submenuText === "Add New FAQ Group") {
        $faqSubmenus.removeClass("current");
        jQuery(this).addClass("current");
      }
    });
  },
  getPostFieldName: function getPostFieldName(index) {
    var fieldName = "helpie_faq_group_items[faq_groups][" + index + "][faq_item][post_id]";
    return fieldName;
  },
  // Get Helpie Faq Groups Selectors
  getSelectors: function getSelectors() {
    return {
      hfaq_group_taxonomy: jQuery("body.taxonomy-helpie_faq_group"),
      repeater_add_btn: ".csf-repeater-add",
      repeater_wrapper: ".csf-repeater-wrapper",
      repeater_items: ".csf-repeater-wrapper .csf-repeater-item",
      repeater: ".csf-field-repeater",
      accordion: ".csf-field-accordion",
      accordion_content: ".csf-repeater-content .csf-accordion-item .csf-accordion-content",
      accordion_post_id_field: ".csf-repeater-content .csf-accordion-item .csf-accordion-content .helpie-group-posts input[type='text']",
      accordion_input_title: ".csf-repeater-content .csf-accordion-item .csf-accordion-content .hfaq-groups__accordion--input-title input[type='text']",
      repeater_clone: ".csf-repeater-wrapper .csf-repeater-item .csf-repeater-helper .csf-repeater-clone",
      fas_quora_icon: '<i class="csf--icon fa fa-quora"></i>',
      pending_status_notice: ' <span class="post-status--pending">(Pending)</span>'
    };
  },
  loadFaqGroupAccordions: function loadFaqGroupAccordions() {
    var thisModule = this;
    var hfaqGroupSelector = thisModule.getSelectors(); // Get all repeaterItems

    var $repeaterItems = hfaqGroupSelector.hfaq_group_taxonomy.find(hfaqGroupSelector.repeater_items); // if the repeaters length is 0 then, current page is creating of faq-group, else editing faq-group page.

    if ($repeaterItems.length == 0) {
      return;
    } // get font-awasome icon


    var fas_quora_icon = hfaqGroupSelector.fas_quora_icon;
    $repeaterItems.each(function () {
      var title = jQuery(this).find(hfaqGroupSelector.accordion_input_title).val();
      var postID = jQuery(this).find(hfaqGroupSelector.accordion_post_id_field).val();
      var isPendingStatus = thisModule.checkPendingStatus(postID);
      titleContent = fas_quora_icon + title;
      /** concat the pending status notice if the post status is pending */

      if (isPendingStatus == true) {
        titleContent = titleContent + hfaqGroupSelector.pending_status_notice;
      }

      thisModule.setAccordionTitle(jQuery(this), titleContent);
    });
  },
  setAccordionTitle: function setAccordionTitle($repeater, titleContent) {
    var $title = $repeater.find(".hfaq-groups__accordion .csf-accordion-item .csf-accordion-title");
    $title.html(titleContent);
  },
  checkPendingStatus: function checkPendingStatus(postID) {
    var status = false;
    postID = parseInt(postID);

    if (this.pending_post_ids.indexOf(postID) > -1) {
      status = true;
    }

    return status;
  }
};
module.exports = FaqGroups;

/***/ }),

/***/ "./assets/js/components/insights/insights.js":
/*!***************************************************!*\
  !*** ./assets/js/components/insights/insights.js ***!
  \***************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

/* Triggered by FAQ events, integrates with Insights */
var Insights = {
  init: function init() {
    var thisModule = this; // console.log(HelpieFaqInsights);
    // Return and Don't load in other pages except insights page

    if (jQuery('.ct-chart').length < 1) {
      return;
    } // console.log('HelpieFaqInsights: ');
    // console.log(HelpieFaqInsights);


    this.week();
    this.month();
    this.year();
    jQuery('.helpie-faq.dashboard input[type=radio] + label').on('click', function (event) {
      var index = jQuery(this).prev('input').attr('id').match(/\d+/)[0];
      var content = jQuery('#content' + index);
      content.find('.ct-chart').each(function (i, e) {
        if (index == 2) {
          thisModule.month();
        }

        if (index == 3) {
          thisModule.year();
        }
      });
    });
  },
  week: function week() {
    // var labels = ['Jan 30', 'Jan 31', 'Feb 1', 'Feb 2', 'Feb 3', 'Feb 4', 'Feb 5'];
    // var values = [5, 9, 7, 8, 5, 3, 5];
    var array_length = HelpieFaqInsights.click.last_30days.labels.length;
    var num_of_values = 7;
    var labels = HelpieFaqInsights.click.last_30days.labels.slice(Math.max(array_length - num_of_values, 1));
    var clickValues = HelpieFaqInsights.click.last_30days.values.slice(Math.max(array_length - num_of_values, 1));
    var searchValues = HelpieFaqInsights.search.last_30days.values.slice(Math.max(array_length - num_of_values, 1));
    this.weekChart = new Chartist.Line('.ct-chart-7day', {
      labels: labels,
      series: [clickValues, searchValues]
    }, {
      axisY: {
        onlyInteger: true
      },
      showArea: true
    });
  },
  month: function month() {
    // console.log("Month Chart ");
    var labels = HelpieFaqInsights.click.last_30days.labels;
    var clickValues = HelpieFaqInsights.click.last_30days.values;
    var searchValues = HelpieFaqInsights.search.last_30days.values;
    this.monthChart = new Chartist.Line('.ct-chart-30day', {
      labels: labels,
      series: [clickValues, searchValues]
    }, {
      axisX: {
        labelInterpolationFnc: function skipLabels(value, index) {
          return index % 5 === 0 ? value : null;
        }
      },
      axisY: {
        onlyInteger: true
      },
      showArea: true
    });
  },
  year: function year() {
    // console.log("Year Chart ");
    var labels = HelpieFaqInsights.click.last_year.labels;
    var clickValues = HelpieFaqInsights.click.last_year.values;
    var searchValues = HelpieFaqInsights.search.last_year.values;
    this.yearChart = new Chartist.Line('.ct-chart-year', {
      labels: labels,
      series: [clickValues, searchValues]
    }, {
      axisY: {
        onlyInteger: true
      },
      showArea: true
    });
  }
};
module.exports = Insights;

/***/ }),

/***/ "./assets/js/settings/styles.js":
/*!**************************************!*\
  !*** ./assets/js/settings/styles.js ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports) {

var selectors = {
  faqSettings: ".helpie-faq__settings",
  theme: ".faq_fields--theme",
  backgrounds: ".faq_fields--color_group",
  themeField: ".csf-fieldset select[name*='theme']",
  searchBgColorField: ".faq_fields--search_background_color",
  searchFontColorField: ".faq_fields--search_font_color",
  searchIconColorField: ".faq_fields--search_icon_color",
  displayModeField: ".faq_fields--display_mode",
  borderField: ".faq_fields--accordion_border",
  headerPaddingsField: ".faq_fields--accordion_header_spacing",
  bodyPaddingsField: ".faq_fields--accordion_body_spacing",
  headerBackgroudColorField: ".wp-picker-container .wp-picker-input-wrap input[name*='header']",
  bodyBackgroudColorField: ".wp-picker-container .wp-picker-input-wrap input[name*='body']",
  inputFields: ".csf-fieldset .csf--inputs .csf--input"
};
var Styles = {
  init: function init() {
    if (jQuery(selectors.faqSettings).length == 0) {
      return false;
    }

    this.events();
  },
  events: function events() {
    var DisplayModeGlobalValue = jQuery(selectors.displayModeField).find(".csf-fieldset select[name*='display_mode']").val();
    var self = this;
    jQuery(selectors.theme).on("change", selectors.themeField, function () {
      var theme = jQuery(this).val();
      var themeBackgrounds = self.getDefaultThemeValues(theme);
      self.setBackgrounds(themeBackgrounds);
    });
    /** ser the border fields values as 0, if the display_mode option is "faq_list"  */

    jQuery(selectors.displayModeField).on("change", ".csf-fieldset select[name*='display_mode']", function () {
      var displayMode = jQuery(this).val();
      var dontSetPaddingStyleValues = DisplayModeGlobalValue != "faq_list" && displayMode != "faq_list" ? true : false;

      if (!dontSetPaddingStyleValues) {
        DisplayModeGlobalValue = displayMode;
        self.setHeaderAndBodyPaddingValues(displayMode);
      }
      /** Set the borders inputs values as zero, if the display is "faq_list" */


      if (displayMode === "faq_list") {// self.setBorderFieldValuesAsZero();
      }
    });
  },
  getDefaultThemeValues: function getDefaultThemeValues(theme) {
    if (theme == "") {
      theme = "light";
    }

    var defaults = {
      light: {
        header: "#FFFFFF",
        body: "#FCFCFC",
        search: {
          background: "#FFFFFF",
          icon: "#171717",
          font_color: "#171717"
        }
      },
      dark: {
        header: "#171717",
        body: "#272727",
        search: {
          background: "#171717",
          icon: "#fcfcfc",
          font_color: "#fcfcfc"
        }
      }
    };
    return defaults[theme];
  },
  setBackgrounds: function setBackgrounds(themeArgs) {
    /** Get the header & body color picker fields  */
    var backgroundFields = jQuery(selectors.backgrounds).find(".csf-fieldset .csf-field-color");

    if (backgroundFields.length == 0) {
      return;
    }

    var self = this;
    var $header = jQuery(backgroundFields[0]);
    var $body = jQuery(backgroundFields[1]);
    /** check & remove the transparent class, if specific element has a background as transparent */

    self.removeTransparentClass($header);
    self.removeTransparentClass($body);
    /** show the header & body backgrounds*/

    self.setBackground($header, themeArgs);
    self.setBackground($body, themeArgs);
    /** set header & body backgrounds input values  */

    self.setInput($header, "header", themeArgs);
    self.setInput($body, "body", themeArgs);
    self.setColorsInSearchFields(themeArgs);
  },
  setColorsInSearchFields: function setColorsInSearchFields(themeArgs) {
    if (helpie_faq_object.plan == "free") {
      return;
    }

    var $searchBgColorField = jQuery(selectors.searchBgColorField).find(".csf-fieldset");
    var $searchIconField = jQuery(selectors.searchIconColorField).find(".csf-fieldset");
    var $searchFontColorField = jQuery(selectors.searchFontColorField).find(".csf-fieldset");
    /** search background color */

    $searchBgColorField.find(".wp-color-result").css("background-color", themeArgs.search.background);
    $searchBgColorField.find("input[name*='search_background_color']").val(themeArgs.search.background);
    /** search icon color */

    $searchIconField.find(".wp-color-result").css("background-color", themeArgs.search.icon);
    $searchIconField.find("input[name*='search_icon_color']").val(themeArgs.search.icon);
    /** search font color */

    $searchFontColorField.find(".wp-color-result").css("background-color", themeArgs.search.font_color);
    $searchFontColorField.find("input[name*='search_font_color']").val(themeArgs.search.font_color);
  },
  removeTransparentClass: function removeTransparentClass($element) {
    var $wpPickerContainer = $element.find(".wp-picker-container");

    if ($wpPickerContainer.hasClass("csf--transparent-active")) {
      $wpPickerContainer.removeClass("csf--transparent-active");
    }
  },
  setBackground: function setBackground($element, themeArgs) {
    $element.find(".wp-picker-container .wp-color-result").css("background-color", themeArgs.header);
  },
  setInput: function setInput($element, inputName, themeArgs) {
    var inputValue = themeArgs.header;
    var inputField = selectors.headerBackgroudColorField;

    if (inputName == "body") {
      inputValue = themeArgs.body;
      inputField = selectors.bodyBackgroudColorField;
    }

    $element.find(inputField).val(inputValue);
  },
  setBorderFieldValuesAsZero: function setBorderFieldValuesAsZero() {
    var inputElement = selectors.inputFields + " input[type='number']";
    jQuery(selectors.borderField).find(inputElement).val(0);
  },
  setHeaderAndBodyPaddingValues: function setHeaderAndBodyPaddingValues(displayMode) {
    var self = this;
    var defaultPaddingValues = self.getDefaultAccordionPaddingValues(displayMode);
    var headerPaddings = defaultPaddingValues.header;
    var bodyPaddings = defaultPaddingValues.body;
    /** set header padding values */

    for (var type in headerPaddings) {
      var propertyValue = headerPaddings[type];
      var inputElement = selectors.inputFields + " input[name*='" + type + "']";
      jQuery(selectors.headerPaddingsField).find(inputElement).val(propertyValue);
    }
    /** set body padding values */


    for (var type in bodyPaddings) {
      var _propertyValue = bodyPaddings[type];

      var _inputElement = selectors.inputFields + " input[name*='" + type + "']";

      jQuery(selectors.bodyPaddingsField).find(_inputElement).val(_propertyValue);
    }
  },
  getDefaultAccordionPaddingValues: function getDefaultAccordionPaddingValues(displayMode) {
    var defaults = {
      faq_list: {
        header: {
          top: "5",
          bottom: "5",
          left: "15",
          right: "15"
        },
        body: {
          top: "5",
          bottom: "5",
          left: "15",
          right: "15"
        }
      },
      others: {
        header: {
          top: "15",
          bottom: "15",
          left: "15",
          right: "15"
        },
        body: {
          top: "15",
          bottom: "0",
          left: "15",
          right: "15"
        }
      }
    };
    return displayMode === "faq_list" ? defaults["faq_list"] : defaults["others"];
  }
};
module.exports = Styles;

/***/ })

/******/ });
//# sourceMappingURL=admin.bundle.js.map