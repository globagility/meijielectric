<?php

namespace HelpieFaq\Features\Notices;

if (!class_exists('\HelpieFaq\Features\Notices\View')) {
    class View
    {
        public function get_content($args)
        {

            $group_id = isset($args['group_id']) && !empty($args['group_id']) && intval($args['group_id']) ? $args['group_id'] : '';
            // $group_link = admin_url("edit-tags.php?taxonomy=helpie_faq_group&post_type=helpie_faq");
            global $post;
            $post_id = isset($post) ? $post->ID : '';
            if (empty($group_id) || empty($post_id)) {
                return;
            }
            $model = new \HelpieFaq\Features\Notices\Model();
            $group = $model->get_group($group_id);
            if (empty($group)) {
                return;
            }

            $group_link = admin_url("term.php?taxonomy=helpie_faq_group&tag_ID={$group_id}&post_type=helpie_faq");

            $edit_post_link = admin_url("post.php?post={$post_id}&action=edit");

            $html = '';
            $html = '<div class="helpie-notices helpie-notices--info">';
            $html .= '<p class="helpie-notices__text"><b>Notice to Admin</b> (only admin can see this)</p>';
            $html .= '<p class="helpie-notices__text">These FAQs are from the <b>"' . $group->name . '"</b> Group. You can edit this FAQ Group <a class="helpie-notices__link" href="' . $group_link . '">here</a>.</p>';
            $html .= '<p class="helpie-notices__text">If you want to display all the FAQs from multiple groups then remove the group id from the shortcode <a class="helpie-notices__link" href="' . $edit_post_link . '">here</a>.</p>';
            $html .= '<p class="helpie-notices__text">To remove this <b>admin-only</b> notice 1) Edit this page 2) Remove the shortcode [helpie_notices group_id="' . $group_id . '"]</p>';
            $html .= '</div>';

            return $html;
        }
    }
}