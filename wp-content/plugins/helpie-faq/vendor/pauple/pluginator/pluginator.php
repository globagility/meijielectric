<?php

// if (!defined('ABSPATH')) {
//     exit; // Exit if accessed directly.
// }

define('PLUGINATOR__FILE__', __FILE__);
define('PLUGINATOR_PATH', dirname(PLUGINATOR__FILE__));
define('PLUGINATOR_SRC_PATH', dirname(PLUGINATOR__FILE__) . '/src');

pluginator_setup_autoload();
// pluginator_load_modules();


function pluginator_setup_autoload()
{
    // require_once PLUGINATOR_PATH . '/autoloader.php';
    // \Pluginator\Autoloader::run();
    require __DIR__ . '/vendor/autoload.php';
}


function pluginator_load_modules()
{

    require_once PLUGINATOR_PATH . '/src/core/upgrader.php';
}
